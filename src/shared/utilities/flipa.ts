const countries = {
    "CB-CH": 'CB-CH',
    "CB-FR": 'CB-FR',
} as const;

type Keys = keyof typeof countries;

const metric = {
    "Product Sell out": "Product Sell out",
    "Registered Machine": "Registered Machine"
};

type Metrics = keyof typeof metric;

const ProductCategories = {
    "Coffee Machine": "Coffee Machine",
    "Machine à café": "Machine à café",
    "promotion": "promotion",
    "free_product_flow_gift": "free_product_flow_gift",
    "Coffee Balls": "Coffee Balls",
    "Coffee Accessories": "Coffee Accessories",
    "machine_registration_gift": "machine_registration_gift",
};

type Category = keyof typeof ProductCategories;

export interface Flipa {
    year?: string,
    country?: Keys,
    "SFTP-CB-CH"?: Partial<Record<Metrics, Partial<Record<Category, Objective>>>>,
    "Shopware-CB-CH"?: Partial<Record<Metrics, Partial<Record<Category, Objective>>>>,
    "Shopware-CB-FR"?: Partial<Record<Metrics, Partial<Record<Category, Objective>>>>,
    "Registered Machine"?: Objective,
    "Customers"?: Objective,
    "Churn rate"?: Objective,
    "Opt In"?: Objective

}

export interface Objective {
    quantity?: number | undefined,
    percentage?: number | undefined,
    multiplier?: number | undefined,
    divider?: number | undefined,
    unit?: string
}

export const FIPLA: Flipa[] = [
    {
        year: '2022',
        country: "CB-CH",
        "SFTP-CB-CH": {
            "Product Sell out": {
                "Coffee Machine": {
                    quantity: 30.5,
                    multiplier: 1000,
                    unit: "Thousan Machines"
                },
                "Coffee Balls": {
                    quantity: 364.3,
                    multiplier: 1000,
                    unit: "Thousan boxes of 9 balls"
                }
            }
        },
        "Shopware-CB-CH": {
            "Product Sell out": {
                "Coffee Machine": {
                    quantity: 19.5,
                    multiplier: 1000,
                    unit: "Thousan Machines"
                },
                "Coffee Balls": {
                    quantity: 22.2,
                    multiplier: 1000,
                    unit: "Thousan boxes of 9 balls"
                }
            }
        },
        "Registered Machine": {
            quantity: 14.6,
            percentage: 75,
            multiplier: 1000,
            unit: "percentage"
        },
        Customers: {
            percentage: 100,
            unit: "percentage"
        },
        "Churn rate": {
            percentage: 0,
            unit: "percentage"
        },
        "Opt In": {
            percentage: 70,
            unit: "percentage"
        }
    },
    {
        year: '2022',
        country: "CB-FR",
        "Shopware-CB-FR": {
            "Product Sell out": {
                "Machine à café": {
                    quantity: 3,
                    multiplier: 1000,
                    unit: "Thousan Machines"
                },
                "Coffee Balls": {
                    quantity: 31.1,
                    multiplier: 1000,
                    unit: "Thousan boxes of 9 balls"
                }
            }
        },
        "Registered Machine": {
            quantity: 1,
            percentage: 75,
            multiplier: 1000,
            unit: "percentage"
        },
        Customers: {
            percentage: 100,
            unit: "percentage"
        },
        "Churn rate": {
            percentage: 0,
            unit: "percentage"
        },
        "Opt In": {
            percentage: 70,
            unit: "percentage"
        }
    }
]