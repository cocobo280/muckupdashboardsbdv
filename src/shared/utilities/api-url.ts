export const apiEndPoint = {    
    url: "https://app.sidis.ai/auto/api/",
    local: "http://192.168.1.8:3000/",
    public:"https://app.sidis.ai/auto/api/",
    n8n:"https://app.sidis.ai/auto/workflow",
    baseUrl: "https://app.sidis.ai/auto/"
  };