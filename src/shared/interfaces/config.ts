export interface Config {
    _id:             IDClass;
    migrated:        boolean;
    createdAt:       AtedAt;
    updatedAt:       AtedAt;
    translations:    Translation[];
    lastUserId:      IDClass;
    lastUserName:    string;
    schema:          Schema[];
    navigationTree:  NavigationTree[];
    endPoints:       EndPoints;
    kickstart:       Kickstart;
    generalSettings: GeneralSettings;
}

export interface IDClass {
    $oid: string;
}

export interface AtedAt {
    $date: Date;
}

export interface EndPoints {
    url:     string;
    public:  string;
    n8n:     string;
    baseUrl: string;
}

export interface GeneralSettings {
    currencyCode:       string;
    currencydisplay:    string;
    currencydigitsInfo: string;
    currencylocale:     string;
    phoneCountries:     string[];
    phoneCountry:       string;
}

export interface Kickstart {
    backGround:   string;
    logo:         string;
    name:         string;
    copyright:    string;
    defaultRoute: string;
    sidebarLogo:  string;
    title:        string;
    favicon:      string;
}

export interface NavigationTree {
    label:      string;
    icon:       string;
    items:      Item[];
    checkAuth?: CheckAuth;
    visible?:   boolean;
}

export interface CheckAuth {
    enabled:    boolean;
    module:     string;
    permission: Permission;
}

export enum Permission {
    Create = "create",
    Read = "read",
}

export interface Item {
    label:                    string;
    icon?:                    string;
    visible?:                 boolean;
    routerLink?:              string[];
    checkAuth?:               CheckAuth;
    routerLinkActiveOptions?: boolean;
    items?:                   Item[];
    checkAdmin?:              CheckAdmin;
}

export interface CheckAdmin {
    enabled: boolean;
}

export interface Schema {
    title:      string;
    name:       string;
    modules:    Module[];
    otherView?: OtherView[];
}

export interface Module {
    title:      string;
    name:       string;
    apiurl:     string;
    actions:    FormElement[];
    form:       PurpleForm;
    list:       ModuleList;
    lookup?:    ModuleLookup;
    relations?: { [key: string]: Relation };
    calendar?:  ModuleCalendar;
    kanban?:    ModuleKanban;
}

export interface FormElement {
    title: string;
    name:  FormName;
}

export enum FormName {
    Create = "create",
    DesignaçãoSocial = "Designação Social",
    List = "list",
    NombreFiscal = "Nombre fiscal",
    SocialDenomination = "Social denomination",
}

export interface ModuleCalendar {
    title?:      string;
    module:      string;
    fromTo:      boolean;
    filterField: string;
    event:       CalendarEvent;
}

export interface CalendarEvent {
    id:          IDEnum;
    title:       string;
    description: string;
    start:       string;
    end:         string;
}

export enum IDEnum {
    Condition = "condition",
    FinancingType = "financingType",
    ID = "id",
    Type = "type",
    Vendor = "vendor",
}

export interface PurpleForm {
    sections: Section[];
}

export interface Section {
    title:           string;
    fields:          SectionField[];
    condition?:      Array<string[] | ConditionClass> | ConditionClass;
    name?:           string;
    module?:         string;
    filter?:         string;
    dataGridFields?: Date1[];
}

export interface ConditionClass {
    relatedTo: IDEnum;
    operator:  Operator;
    value:     string;
}

export enum Operator {
    Empty = "!=",
    Operator = "==",
    Purple = "=!",
}

export interface Date1 {
    label: string;
    name:  string;
}

export interface SectionField {
    title?:                string;
    name?:                 string;
    validator?:            Validator[];
    type:                  FieldType;
    initialValue?:         string[] | boolean | number | string;
    options?:              Option[];
    visible:               boolean | VisibleClass;
    disabled?:             boolean | ConditionClass;
    filterable?:           boolean;
    min?:                  number | string;
    max?:                  number | null | string;
    showTime?:             boolean;
    event?:                FieldEvent;
    query?:                Query;
    lookup?:               boolean | LookupLookup | string;
    latLong?:              boolean;
    configExternalValues?: ConfigExternalValues;
    changeOtherField?:     ChangeOtherField;
    subtype?:              string;
    list?:                 boolean;
    parent?:               string;
    operation?:            Operation;
}

export interface ChangeOtherField {
    relatedTo: RelatedToRelatedTo[] | RelatedToRelatedTo;
}

export enum RelatedToRelatedTo {
    Description = "description",
    Name = "name",
}

export interface ConfigExternalValues {
    separation: string;
    order:      string[];
}

export interface FieldEvent {
    name:       ValueEnum;
    templates?: Templates;
    relatedTo?: EventRelatedTo[];
    value?:     ValueEnum;
}

export enum ValueEnum {
    Concat = "concat",
    DaysBetween = "daysBetween",
    History = "history",
    Operation = "operation",
}

export enum EventRelatedTo {
    AcquisitionDate = "acquisitionDate",
    Phase = "phase",
    TransactionDate = "transactionDate",
}

export interface Templates {
    id: string;
}

export interface LookupLookup {
    name:      string;
    relatedTo: RelatedToClass;
    multiple?: boolean;
    filter?:   LookupFilter;
}

export interface LookupFilter {
    where: PurpleWhere;
}

export interface PurpleWhere {
    stage?:      WhereStage;
    statusName?: string;
}

export interface WhereStage {
    neq: string;
}

export interface RelatedToClass {
    parent:  string[];
    child:   string[];
    fields:  Date1[];
    loadOn?: string;
}

export interface Operation {
    valueA:   string;
    valueB:   string;
    result:   string;
    operator: string;
}

export interface Option {
    label: string;
    value: boolean | string;
}

export interface Query {
    relatedTo: QueryRelatedTo[];
}

export enum QueryRelatedTo {
    Email = "email",
    Plate = "plate",
    Vat = "vat",
}

export enum FieldType {
    Address = "address",
    Anchor = "anchor",
    Date = "date",
    Empty = "",
    HTML = "html",
    List = "list",
    MultiSelect = "multiSelect",
    Number = "number",
    Object = "object",
    Password = "password",
    Phoneimput = "phoneimput",
    Provisional = "provisional",
    PurpleEmpty = "Empty",
    Query = "query",
    Radio = "radio",
    Select = "select",
    Subtitle = "subtitle",
    TemplateEmpty = "empty",
    Text = "text",
    Textarea = "textarea",
    View = "view",
}

export enum Validator {
    Email = "email",
    NifValid = "nifValid",
    Required = "required",
}

export interface VisibleClass {
    relatedTo: string;
    operator:  Operator;
    value:     boolean | string;
}

export interface ModuleKanban {
    field:      string;
    cardFields: CardFields;
    priority:   KanbanPriority;
}

export interface CardFields {
    title:       Date1;
    date1:       Date1;
    date2:       Date1;
    description: Date1;
    type:        Date1;
    priority:    Date1;
}

export interface KanbanPriority {
    name:    string;
    options: Options;
}

export interface Options {
    high?:        string;
    medium?:      string;
    low?:         string;
    open?:        string;
    in_process?:  string;
    closed?:      string;
    stopped?:     string;
    sale?:        string;
    service?:     string;
    maintenance?: string;
}

export interface ModuleList {
    title:       string;
    name:        string;
    filter:      ListFilter;
    fields:      ListField[];
    openNewTab?: boolean;
    title2?:     string;
    view?:       string;
    path?:       string;
}

export interface ListField {
    label?:    string;
    name:      string;
    type:      TypeEnum;
    width:     string;
    template?: PurpleTemplate;
    title?:    string;
}

export enum PurpleTemplate {
    Address = "address",
    Date = "date",
    Email = "email",
    Name = "name",
    Percentage = "percentage",
    Phone = "phone",
    Price = "price",
    PropertyEffectiveness = "propertyEffectiveness",
    Short = "short",
}

export enum TypeEnum {
    Date = "date",
    Empty = "",
    Numeric = "numeric",
}

export interface ListFilter {
    limit:  number;
    where?: FluffyWhere;
}

export interface FluffyWhere {
    or?:  Or[];
    and?: And[];
}

export interface And {
    stage?:      AndStage;
    statusName?: string;
}

export interface AndStage {
    nin?:    string[];
    exists?: boolean;
}

export interface Or {
    stage: StageStage | string;
}

export interface StageStage {
    exists: boolean;
}

export interface ModuleLookup {
    title:  Title;
    name:   string;
    fields: LookupField[];
}

export interface LookupField {
    label:     string;
    name:      string;
    template?: FieldType;
}

export enum Title {
    Name = "name",
    Roles = "Roles",
    Templates = "Templates",
}

export interface Relation {
    scope:  ScopeClass;
    module: string;
}

export interface ScopeClass {
}

export interface OtherView {
    title: string;
    link:  string;
    name?: string;
}

export interface Translation {
    language:        Language;
    inbox:           Inbox;
    alerts:          Alerts;
    general:         TranslationGeneral;
    actions:         TranslationActions;
    login:           Login;
    sidebar:         Sidebar;
    dashboard:       TranslationDashboard;
    users:           Users;
    offices:         Offices;
    leads:           Leads;
    contacts:        Contacts;
    peoples:         Peoples;
    products:        Products;
    transactions:    Transactions;
    activities:      TranslationActivities;
    cases:           TranslationCases;
    settings:        TranslationSettings;
    quotes:          Quotes;
    communications:  Communications;
    companies:       Companies;
    profile:         Profile;
    calendar:        TranslationCalendar;
    scheduler:       Scheduler;
    grid:            Grid;
    kanban:          TranslationKanban;
    reports:         Reports;
    campaigns:       Campaigns;
    "contact-lists": ContactLists;
    segments:        Segments;
    templates:       Segments;
    employees:       Employees;
    roles:           Roles;
    properties:      Properties;
    "web-analytics": WebAnalytics;
    role:            Role;
}

export interface TranslationActions {
    name:                 string;
    bulkName?:            string;
    stage:                string;
    lead:                 string;
    leadValidation:       string;
    leadValidationTitle?: string;
    contact:              string;
    employee:             string;
    client:               string;
    delete:               string;
    softDelete:           string;
    bulkEdit:             string;
    bulkSoftDelete:       string;
    undelete:             string;
    export:               string;
    edit:                 ActionsEdit;
}

export interface ActionsEdit {
    title:        string;
    name:         string;
    peoples:      string;
    employees:    string;
    activities:   string;
    cases:        string;
    companies:    string;
    offices:      string;
    quotes:       string;
    transactions: string;
    products:     string;
    users:        string;
}

export interface TranslationActivities {
    form:    ActivitiesForm;
    list:    ActivitiesList;
    new:     NewClass;
    edit:    NewClass;
    message: ActivitiesMessage;
}

export interface NewClass {
    title: string;
}

export interface ActivitiesForm {
    title2:              string;
    title3:              string;
    title4:              string;
    from:                string;
    to:                  string;
    dateContact?:        string;
    nextContactDate:     string;
    location:            string;
    comments:            string;
    lastComments:        string;
    commentsNextContact: string;
    client:              string;
    status:              string;
    latitude:            string;
    longitude:           string;
    newPrice1:           string;
    newPrice2:           string;
    subject:             Subject;
    followUpType?:       FollowUpType;
    type:                PurpleType;
    statusS:             StatusS;
    priority:            FormPriority;
    activityName:        string;
    contactingDate?:     string;
    salesPortfolio?:     FollowUpType;
}

export interface FollowUpType {
    title: string;
    v08:   string;
    v10:   string;
    v11:   string;
    v12:   string;
    v28:   string;
    g09:   string;
    xxx:   string;
}

export interface FormPriority {
    high:   string;
    medium: string;
    low:    string;
}

export interface StatusS {
    open:      string;
    closed:    string;
    stopped:   string;
    inProcess: string;
}

export interface Subject {
    title: string;
    v1:    string;
    v2:    string;
    v3:    string;
    ot:    string;
}

export interface PurpleType {
    title:          string;
    none:           string;
    price_update:   string;
    mail:           string;
    call:           string;
    event:          string;
    security:       string;
    transaction:    string;
    openHous:       string;
    other:          string;
    republications: string;
    visit:          string;
    whatsapp:       string;
    socialMedia:    string;
    sms:            string;
}

export interface ActivitiesList {
    title:        string;
    myTitle:      string;
    toDoTitle:    string;
    createButton: string;
}

export interface ActivitiesMessage {
    fieldsError:    string;
    success:        string;
    editSuccess:    string;
    continue?:      string;
    continueCreate: string;
    exit?:          string;
    delete:         string;
    deleteConfirm:  string;
}

export interface Alerts {
    accessDenied: string;
    importer:     Importer;
    eraser:       Eraser;
}

export interface Eraser {
    success: string;
}

export interface Importer {
    invalidDates: string;
}

export interface TranslationCalendar {
    createdAt:       string;
    transactionDate: string;
    firstDayOfWeek:  number;
    dayNames:        string[];
    dayNamesShort:   string[];
    dayNamesMin:     string[];
    monthNames:      string[];
    monthNamesShort: string[];
    today:           string;
    clear:           string;
    dateFormat:      string;
    weekHeader:      string;
    datePicker:      DatePicker;
}

export interface DatePicker {
    placeholder: string;
}

export interface Campaigns {
    form: CampaignsForm;
    list: NewClass;
}

export interface CampaignsForm {
    title1:             string;
    title:              string;
    status:             PurpleStatus;
    start:              string;
    end:                string;
    channel:            string;
    channels:           Channels;
    type:               string;
    types:              PurpleTypes;
    cost:               string;
    revenue:            string;
    topic:              Topic;
    title2:             string;
    segments:           string;
    dateTime:           string;
    senderName:         string;
    subject:            string;
    processedCount:     string;
    isStarred:          string;
    testEmail:          string;
    template:           string;
    title3:             string;
    newLeads:           string;
    actualCost:         string;
    newContacts:        string;
    actualRevenue:      string;
    newClients:         string;
    revenueInPipeline:  string;
    contributedDeals:   string;
    lastActivity:       string;
    title4:             string;
    deliveredCount:     string;
    blockedCount:       string;
    queuedCount:        string;
    spamAsScore:        string;
    openedCount:        string;
    spamComplaintCount: string;
    clickedCount:       string;
    unsubscribedCount:  string;
    bouncedCount:       string;
    deferredCount:      string;
    hardBouncedCount:   string;
    softBouncedCount:   string;
    title5:             string;
    unsubscribedAt:     string;
    blockedAt:          string;
    markedAsSpamAt:     string;
    bouncedAt:          string;
    opened:             string;
    clicked:            string;
}

export interface Channels {
    textMessage: string;
    email:       string;
    whatsApp:    string;
    facebookAds: string;
    facebook:    string;
    googleAds:   string;
    linkedInAds: string;
    others:      string;
}

export interface PurpleStatus {
    name:     string;
    draft:    string;
    sent:     string;
    pending:  string;
    active:   string;
    finished: string;
    inQueue:  string;
}

export enum Topic {
    Arquivos = "Arquivos",
    List = "List",
    Lista = "Lista",
    Listado = "Listado",
    Listar = "Listar",
    Lists = "Lists",
}

export interface PurpleTypes {
    publicRelations:      string;
    partners:             string;
    referralProgram:      string;
    partnerProgram:       string;
    advertisement:        string;
    directMail:           string;
    email:                string;
    telemarketing:        string;
    textMessageMarketing: string;
    GPDR:                 string;
}

export interface TranslationCases {
    form:    CasesForm;
    list:    CasesList;
    new:     NewClass;
    edit:    PurpleEdit;
    filter:  CasesFilter;
    message: CasesMessage;
}

export interface PurpleEdit {
    title:               string;
    successTitle?:       string;
    successDescription?: string;
}

export interface CasesFilter {
    startsWithOperator:    string;
    endsWithOperator:      string;
    containsOperator:      string;
    beforeOrEqualOperator: string;
    beforeOperator:        string;
    afterOrEqualOperator:  string;
    afterOperator:         string;
    notEqOperator:         string;
    notContainsOperator:   string;
    lteOperator:           string;
    ltOperator:            string;
    isTrue:                string;
    isFalse:               string;
    isNullOperator:        string;
    isNotNullOperator:     string;
    isNotEmptyOperator:    string;
    isEmptyOperator:       string;
    gteOperator:           string;
    gtOperator:            string;
    eqOperator:            string;
}

export interface CasesForm {
    title:            string;
    title2:           string;
    title3:           string;
    title10:          string;
    closingDate:      string;
    statusN:          string;
    makeExchange:     string;
    yearExchange:     string;
    modelExchange:    string;
    gearExchange:     string;
    colorExchange:    string;
    kmExchange:       string;
    vinExchange:      string;
    condition:        string;
    conditionSelect:  ConditionSelect;
    accident:         string;
    accidentSelect:   AccidentSelect;
    dataTestDrive:    string;
    referrerURL:      string;
    noTicket:         string;
    noCase:           string;
    motiveCase:       string;
    originCase:       string;
    priority:         string;
    coment:           string;
    affair:           string;
    agent:            string;
    offerAmount:      string;
    property:         string;
    transaction:      string;
    opportunity:      string;
    amountUS:         string;
    amountBS:         string;
    seller:           string;
    buyer:            string;
    sellerAgent:      string;
    catcherAgent:     string;
    swal:             FormSwal;
    typeName:         string;
    type:             FluffyType;
    motiveCaseSelect: MotiveCaseSelect;
    statusName:       string;
    status:           FluffyStatus;
    originCaseSelect: OriginCaseSelect;
    prioritySelect:   PrioritySelect;
}

export interface AccidentSelect {
    yes:      string;
    no:       string;
    dontKnow: string;
}

export interface ConditionSelect {
    perfect:      string;
    good:         string;
    medium:       string;
    belowAverage: string;
    dontKnow:     string;
}

export interface MotiveCaseSelect {
    none:      string;
    null:      string;
    open:      string;
    closed:    string;
    stopped:   string;
    inProcess: string;
}

export interface OriginCaseSelect {
    none:          string;
    null:          string;
    email:         string;
    event:         string;
    facebook:      string;
    canvas:        string;
    newspaper:     string;
    freePortal:    string;
    sidisWeb:      string;
    twitter:       string;
    realEstateWeb: string;
}

export interface PrioritySelect {
    none:   string;
    null:   string;
    high:   string;
    medium: string;
    low:    string;
}

export interface FluffyStatus {
    none:      string;
    null:      string;
    open:      string;
    closed:    string;
    stopped:   string;
    inProcess: string;
    sold?:     string;
    Unsold?:   string;
    inactive?: string;
}

export interface FormSwal {
    title: string;
    text:  string;
}

export interface FluffyType {
    none:             string;
    offer:            string;
    offer_return:     string;
    test_drive:       string;
    info_request:     string;
    doc_update:       string;
    rent:             string;
    catchment:        string;
    special_request:  string;
    sing_up:          string;
    suggestion_claim: string;
    reservation:      string;
    sale:             string;
    visit:            string;
    chat_ticket:      string;
    privacyAgreement: string;
}

export interface CasesList {
    title:      string;
    myTitle:    string;
    createAt:   string;
    lastUpdate: string;
    noRecords?: string;
}

export interface CasesMessage {
    fieldsError:    string;
    success:        string;
    continueCreate: string;
    editSuccess:    string;
}

export interface Communications {
    messages: string;
    form:     CommunicationsForm;
    list:     NewClass;
}

export interface CommunicationsForm {
    title:         string;
    message:       string;
    subject:       string;
    source:        string;
    state:         string;
    type:          string;
    fromId:        string;
    toId:          string;
    campaignId:    string;
    campaignName:  string;
    toName:        string;
    fromName:      string;
    toEmail:       string;
    fromEmail:     string;
    campaignsId:   string;
    campaignsName: string;
    optOut:        OptOut;
    optIn:         OptIn;
}

export interface OptIn {
    title:         string;
    title2:        string;
    title3:        string;
    name:          string;
    channel:       string;
    channelType:   ChannelType;
    contactLists:  string;
    titleWeb:      string;
    messageWeb:    string;
    buttomConfirm: string;
}

export interface ChannelType {
    textMessage: string;
    email:       string;
    messaging:   string;
    phone:       string;
    post:        string;
}

export interface OptOut {
    title:         string;
    name:          string;
    titleWeb:      string;
    messageWeb:    string;
    errorWeb:      string;
    buttomConfirm: string;
}

export interface Companies {
    form: FormElement;
    list: NewClass;
}

export interface ContactLists {
    form: ContactListsForm;
    list: NewClass;
}

export interface ContactListsForm {
    title:       string;
    title1:      string;
    description: string;
}

export interface Contacts {
    form:    ContactsForm;
    list:    ContactsList;
    new:     ContactsNew;
    edit:    ContactsEdit;
    filter?: CasesFilter;
}

export interface ContactsEdit {
    title: string;
    swal?: FormSwal;
}

export interface ContactsForm {
    title:               string;
    title2:              string;
    title3:              string;
    title4:              string;
    title5:              string;
    title6:              string;
    title7:              string;
    title8:              string;
    title9:              string;
    urlTraking:          string;
    referrerURL:         string;
    utm_source:          string;
    utm_medium:          string;
    utm_term:            string;
    utm_content:         string;
    utm_placement:       string;
    utm_buttom_text:     string;
    campaignTraking:     string;
    gclid:               string;
    fbclid:              string;
    utm_campaign:        string;
    utm_ad:              string;
    utm_ad_name:         string;
    utm_ad_placement:    string;
    utm_ad_adset:        string;
    utm_ad_buttom_text:  string;
    ctaTraking:          string;
    cta_placement:       string;
    cta_buttom_text:     string;
    cta_content:         string;
    cta_type:            string;
    supervisor:          string;
    make:                string;
    haveCar:             string;
    reason:              string;
    satisfaction:        string;
    desiredProduct:      string;
    dateOfChange:        string;
    lastValidation:      string;
    lastTransaction:     string;
    contactedType?:      string;
    vat?:                string;
    typeDniSelect:       TypeDniSelect;
    interests:           Interests;
    secondaryPhone:      string;
    thirdPhone:          string;
    fourthPhone:         string;
    secondaryEmail?:     string;
    dateOfBirth:         string;
    type:                string;
    dni:                 string;
    documentId:          string;
    typeDni:             string;
    sourceLead:          string;
    sourceInterest:      string;
    associatedAgent:     string;
    assignedName:        string;
    contactingDate:      string;
    interestedIn:        string;
    country:             string;
    city:                string;
    state:               string;
    address:             string;
    company:             string;
    businessName:        string;
    description:         string;
    spouse:              string;
    code:                string;
    office:              string;
    relatedContacts:     string;
    contact_types:       ContactTypes;
    inactive:            string;
    contactInactive:     ContactInactive;
    contact_sources:     ContactSources;
    interest_sources:    InterestSources;
    yes_no_options:      YesNoOptions;
    role?:               string;
    callCenterAgent?:    string;
    convertedToContact?: string;
    convertedToLead?:    string;
    convertedToClient?:  string;
}

export interface ContactInactive {
    open: string;
    G09:  string;
    XXX:  string;
    G02:  string;
    G03:  string;
    rgpd: string;
}

export interface ContactSources {
    stand:               string;
    GoogleAds:           string;
    FacebookAds:         string;
    Instagram:           string;
    WhatsApp:            string;
    Facebook:            string;
    LinkedIn:            string;
    Twitter:             string;
    EmailMarketing:      string;
    ExternalReferral:    string;
    InternalReferral:    string;
    SitesGasm:           string;
    SiteMarca:           string;
    FreePortals:         string;
    Eventos:             string;
    Tradeshows:          string;
    Exposicoes:          string;
    Phonecall2:          string;
    Siva:                string;
    Seat:                string;
    Audi:                string;
    Volkswagen:          string;
    Skoda:               string;
    sidisWeb:            string;
    sidisWebChat:        string;
    sidis:               string;
    electricgo:          string;
    gosidis?:            string;
    bex:                 string;
    "RAI - BD Clientes": string;
    autoline?:           string;
    goAsidis?:           string;
}

export interface ContactTypes {
    individual?:    string;
    corporate?:     string;
    government?:    string;
    other?:         string;
    seller:         string;
    manager:        string;
    director:       string;
    receptionist:   string;
    partner:        string;
    intern:         string;
    externalSeller: string;
    employee?:      string;
    Individual?:    string;
    Corporate?:     string;
    Government?:    string;
}

export interface InterestSources {
    FriendOrKnown:        string;
    Banner:               string;
    InformativeTalk:      string;
    ImmobiliaryColleague: string;
    Other:                string;
    WebSkyGroup:          string;
    Newspaper:            string;
    Magazine:             string;
    SKYAdvisorPromotion:  string;
    AlreadyClient:        string;
    Youtube:              string;
    Poster:               string;
    FreeRealEstatePortal: string;
}

export interface Interests {
    title:       string;
    cycling:     string;
    soccer:      string;
    golf:        string;
    canoeing:    string;
    surf:        string;
    music:       string;
    fashion:     string;
    economy:     string;
    travel:      string;
    literature:  string;
    motorsports: string;
    others:      string;
    othersNotes: string;
}

export interface TypeDniSelect {
    citizenCardboard: string;
    bi?:              string;
    passport:         string;
    residenceTitle:   string;
    dni?:             string;
}

export interface YesNoOptions {
    yes: string;
    no:  string;
}

export interface ContactsList {
    title:    string;
    myTitle:  string;
    add:      string;
    title2:   string;
    myTitle2: string;
    add2:     string;
}

export interface ContactsNew {
    title:              string;
    checkContactButtom: string;
    checkLookupHeader:  string;
    checkButtom:        string;
}

export interface TranslationDashboard {
    Weekly:           string;
    Monthly:          string;
    Yearly:           string;
    title:            string;
    nextContactDate:  string;
    config:           ConfigClass;
    legend:           Legend;
    sales:            ContactsClass;
    acquisitions:     NewClass;
    cases:            ContactsClass;
    salesCategory:    NewClass;
    topSales:         NewClass;
    topRoyalty:       NewClass;
    donut:            Donut;
    topCommissions:   NewClass;
    activities:       DashboardActivities;
    catchments:       Catchments;
    requestSource:    RequestSource;
    opportunities:    Opportunities;
    infoRequests:     NewClass;
    lastCatchments:   LastCatchments;
    lastTransactions: NewClass;
    lastActivities:   ContactsClass;
    lastCases:        NewClass;
    lastLeads:        NewClass;
    swal:             DashboardSwal;
    files:            Files;
    funnel:           NewClass;
    contacts:         ContactsClass;
    futureSales:      NewClass;
}

export interface DashboardActivities {
    title:      string;
    lastMonth:  string;
    curtMmonth: string;
}

export interface ContactsClass {
    title:  string;
    title2: string;
}

export interface Catchments {
    title: string;
    card:  CatchmentsCard;
}

export interface CatchmentsCard {
    title:         string;
    effectiveness: string;
    moreThan:      string;
    days:          string;
}

export interface ConfigClass {
    from:       string;
    to:         string;
    currency:   string;
    top:        string;
    profit:     string;
    percentage: string;
    sales:      Sales;
    request:    string;
    print:      string;
}

export interface Sales {
    title: string;
    total: string;
}

export interface Donut {
    title:      string;
    earningsIn: string;
    salesIn:    string;
    total:      string;
    percentage: string;
}

export interface Files {
    sales: string;
}

export interface LastCatchments {
    title:        string;
    name:         string;
    code:         string;
    type:         string;
    category:     string;
    urbanization: string;
}

export interface Legend {
    total:       string;
    sales:       string;
    catchments:  string;
    properties:  string;
    propertyQty: string;
}

export interface Opportunities {
    won:  string;
    open: string;
}

export interface RequestSource {
    title: string;
    card:  RequestSourceCard;
}

export interface RequestSourceCard {
    title:    string;
    requests: string;
    offers:   string;
}

export interface DashboardSwal {
    oops:        string;
    info:        string;
    missingDate: string;
}

export interface Employees {
    list: NewClass;
}

export interface TranslationGeneral {
    noPhone:           string;
    message1:          string;
    people:            string;
    touchpoint:        string;
    companies:         string;
    sales:             string;
    products:          string;
    payments:          string;
    campaings:         string;
    reports:           string;
    files:             string;
    images:            string;
    actions:           string;
    dataSource:        string;
    select:            string;
    cancel:            string;
    confirm:           string;
    continue:          string;
    upload:            string;
    uploadFiles:       string;
    uploadImages:      string;
    typeTheWord:       string;
    infoFiles:         string;
    infoImages:        string;
    configurations:    string;
    createdAt:         string;
    updatedAt:         string;
    deletedAt:         string;
    groupPanelEmpty:   string;
    "dropdown-filter": DropdownFilter;
    import:            string;
    importer:          string;
    editar:            string;
    bulkEdit:          string;
}

export interface DropdownFilter {
    noDataText: string;
}

export interface Grid {
    noRecords:                   string;
    columnsApply:                string;
    columnsReset:                string;
    filterAfterOperator:         string;
    filterAfterOrEqualOperator:  string;
    filterAndLogic:              string;
    filterOrLogic:               string;
    filterBeforeOperator:        string;
    filterBeforeOrEqualOperator: string;
    filterClearButton:           string;
    filterContainsOperator:      string;
    filterNotContainsOperator:   string;
    filterDateToday:             string;
    filterFilterButton:          string;
    filterGtOperator:            string;
    filterGteOperator:           string;
    filterIsEmptyOperator:       string;
    filterIsNotEmptyOperator:    string;
    filterIsNotNullOperator:     string;
    filterIsNullOperator:        string;
    filterLtOperator:            string;
    filterLteOperator:           string;
    filterNotEqOperator:         string;
    filterStartsWithOperator:    string;
    filterEndstsWithOperator:    string;
    sortAscending:               string;
    sortDescending:              string;
}

export interface Inbox {
    send:                string;
    discard:             string;
    title:               string;
    subtitle:            string;
    messenger:           string;
    success:             string;
    noCredentials:       string;
    noCredentials2:      string;
    setNow:              string;
    setLater:            string;
    emailSuccess:        string;
    emailError:          string;
    textMessagesSuccess: string;
    textMessagesError:   string;
}

export interface TranslationKanban {
    activities:   NewClass;
    products:     NewClass;
    transactions: NewClass;
    cases:        NewClass;
    quotes:       NewClass;
    campaigns:    NewClass;
    offices:      NewClass;
    peoples:      NewClass;
}

export interface Language {
    default:  boolean;
    code:     string;
    name:     string;
    currency: string;
}

export interface Leads {
    form: LeadsForm;
    list: LeadsList;
    new:  NewClass;
    edit: NewClass;
}

export interface LeadsForm {
    title:      string;
    validation: YesNoOptions;
}

export interface LeadsList {
    title:   string;
    myTitle: string;
    add:     string;
}

export interface Login {
    title:    string;
    alert:    string;
    rights:   string;
    email:    string;
    password: string;
    es:       string;
    en:       string;
    pt:       string;
}

export interface Offices {
    form: OfficesForm;
    list: OfficesList;
    new:  ScopeClass;
    edit: NewClass;
}

export interface OfficesForm {
    title:               string;
    stand:               string;
    web:                 string;
    relatedTransactions: string;
    companyName:         string;
    relatedProducts:     string;
    relatedtitle:        string;
    mainContact:         string;
    email:               string;
    locationId:          string;
    seller:              string;
    dealership:          string;
    internalCode:        string;
    cae:                 string;
    others:              string;
    industry:            Industry;
    CompanyType:         CompanyType;
    vendor:              PurpleVendor;
}

export interface CompanyType {
    title:        string;
    dealer:       string;
    vendor:       string;
    organization: string;
}

export interface Industry {
    title:      string;
    automotive: string;
    realEstate: string;
    technology: string;
    telecom:    string;
    government: string;
    health:     string;
}

export interface PurpleVendor {
    title:            string;
    vwBank:           string;
    santaderConsumer: string;
    cetelem:          string;
    others:           string;
}

export interface OfficesList {
    title:        string;
    createButton: string;
}

export interface Peoples {
    form:    PeoplesForm;
    list:    ContactsList;
    new:     ContactsNew;
    edit:    ContactsEdit;
    filter?: CasesFilter;
}

export interface PeoplesForm {
    title:               string;
    title2:              string;
    title3:              string;
    title4:              string;
    title5:              string;
    title6:              string;
    title7:              string;
    title8:              string;
    title9:              string;
    title10:             string;
    urlTraking:          string;
    referrerURL:         string;
    utm_source:          string;
    utm_medium:          string;
    utm_term:            string;
    utm_content:         string;
    utm_placement:       string;
    utm_buttom_text:     string;
    campaignTraking:     string;
    gclid:               string;
    fbclid:              string;
    utm_campaign:        string;
    utm_ad:              string;
    utm_ad_name:         string;
    utm_ad_placement:    string;
    utm_ad_adset:        string;
    utm_ad_buttom_text:  string;
    ctaTraking:          string;
    cta_placement:       string;
    cta_buttom_text:     string;
    cta_content:         string;
    cta_type:            string;
    supervisor:          string;
    make:                string;
    haveCar:             string;
    reason:              string;
    satisfaction:        string;
    desiredProduct:      string;
    dateOfChange:        string;
    lastValidation:      string;
    lastTransaction:     string;
    companyName:         string;
    role:                string;
    callCenterAgent:     string;
    convertedToContact?: string;
    convertedToLead?:    string;
    convertedToClient?:  string;
    contactedType:       string;
    contactedTypeList:   ContactedTypeList;
    vat?:                string;
    typeDniSelect:       TypeDniSelect;
    salutationName:      string;
    salutation:          Salutation;
    sex:                 Sex;
    contactPreference:   ContactPreference;
    unsubscribe?:        ContactPreference;
    nextContactDate:     string;
    secondaryPhone?:     string;
    thirdPhone?:         string;
    fourthPhone?:        string;
    secondaryEmail?:     string;
    dateOfBirth:         string;
    type:                string;
    dni:                 string;
    documentId:          string;
    typeDni:             string;
    sourceLead:          string;
    sourceInterest:      string;
    associatedAgent:     string;
    assignedName:        string;
    contactingDate:      string;
    hourContact:         string;
    interestedIn:        string;
    country:             string;
    city:                string;
    state:               string;
    address:             string;
    company:             string;
    businessName:        string;
    description:         string;
    spouse:              string;
    code:                string;
    office:              string;
    relatedContacts:     string;
    contact_types:       ContactTypes;
    inactive:            string;
    contactInactive:     ContactInactive;
    contact_sources:     ContactSources;
    interest_sources:    InterestSources;
    yes_no_options:      YesNoOptions;
}

export interface ContactPreference {
    title:       string;
    title2?:     string;
    email:       string;
    sms:         string;
    chat:        string;
    whatsapp:    string;
    phone:       string;
    socialMedia: string;
    visit?:      string;
}

export interface ContactedTypeList {
    contacted:        string;
    notContacted:     string;
    attemptedContact: string;
    contactInFuture:  string;
}

export interface Salutation {
    mr:         string;
    mrs:        string;
    miss:       string;
    doctor:     string;
    doctorF:    string;
    excellence: string;
    excellency: string;
    engineer:   string;
    architect:  string;
    priest:     string;
}

export interface Sex {
    title:  string;
    male:   string;
    female: string;
    other:  string;
}

export interface Products {
    form:     ProductsForm;
    list:     ProductsList;
    new:      ScopeClass;
    edit:     NewClass;
    messages: Messages;
}

export interface ProductsForm {
    title:                 string;
    title1:                string;
    price?:                string;
    name:                  string;
    plate:                 string;
    plateDate:             string;
    doors:                 string;
    year:                  string;
    mileage:               string;
    cor:                   string;
    seats:                 string;
    autonomy:              string;
    featured:              string;
    vin:                   string;
    serial:                string;
    engineSize:            string;
    kWh:                   string;
    co2:                   string;
    power:                 string;
    fuelName:              string;
    lastVisitDate:         string;
    effectiveDate:         string;
    type:                  TentacledType;
    paymentMethod:         NewClass;
    fuel:                  Fuel;
    metricUnitName:        string;
    metricUnit:            MetricUnit;
    gears:                 string;
    gearboxName:           string;
    gearbox:               Gearbox;
    make:                  string;
    makeType:              { [key: string]: string };
    model:                 string;
    version:               string;
    bodyTypeName:          string;
    bodyType2:             BodyType2;
    bodyType:              BodyType;
    zone:                  string;
    propertyNumber:        string;
    title2:                string;
    houseFloors:           string;
    ground:                string;
    constructionYear:      string;
    privateBathroom:       string;
    environments:          string;
    buildingEnvironments:  string;
    services:              string;
    title3:                string;
    grossArea:             string;
    usefulArea:            string;
    landArea:              string;
    condition:             string;
    typology:              string;
    modCons:               string;
    bedrooms:              string;
    bathrooms:             string;
    floor:                 string;
    street:                string;
    elevator:              string;
    exclusivity:           string;
    description:           string;
    title4:                string;
    equipment:             string;
    equipmentList:         EquipmentList;
    options:               string;
    optionsList:           OptionsList;
    title5:                string;
    sync:                  string;
    published:             string;
    close:                 string;
    property:              string;
    Related:               string;
    Activities:            string;
    Applications:          string;
    Transactions:          string;
    images:                string;
    relatedActivities:     string;
    relatedRequests:       string;
    relatedTransactions:   string;
    attachImage:           string;
    attachImageText:       string;
    attachImageText2:      string;
    attachImageText3:      string;
    image:                 string;
    selectFileButton:      string;
    uploadFileButton:      string;
    cover:                 string;
    propertyType:          string;
    offerType:             string;
    rentalPrice:           string;
    salePrice:             string;
    owner:                 string;
    negotiable:            string;
    catchDate:             string;
    exclusiveness:         string;
    region:                string;
    municipality:          string;
    postalCode:            string;
    exact:                 string;
    advantages?:           string;
    statusName:            string;
    status:                TentacledStatus;
    conditions:            Conditions;
    source:                Source;
    interiorColor:         string;
    color:                 Color;
    vehicle_type:          VehicleType;
    categories:            Categories;
    countries:             Countries;
    vicinity:              Vicinity;
    floorS:                FloorS;
    environmentsS:         EnvironmentsS;
    building_environments: BuildingEnvironments;
    facilitiesS:           FacilitiesS;
    servicesS:             ServicesS;
    offerTypeS:            OfferTypeS;
    additionalDetailsS:    AdditionalDetailsS;
    usage?:                string;
    apartmentFloors?:      string;
    offices?:              string;
    facilities?:           string;
    placesPerFloor?:       string;
    buildingPercentage?:   string;
    condominium?:          string;
    trunk?:                string;
    bsfPrice?:             string;
    pricePerMeter?:        string;
    usdPrice?:             string;
    chargesToBeReleased?:  string;
    suggestedLease?:       string;
    deposit?:              string;
    acquisitionDate?:      string;
    acquisitionValue?:     string;
    automaticExtension?:   string;
    additionalDetails?:    string;
    financing?:            string;
    observations?:         string;
    coveredTerrace?:       string;
    uncoveredTerrace?:     string;
    uncoveredArea?:        string;
    gardenArea?:           string;
    underConstruction?:    string;
    coveredArea?:          string;
    totalArea?:            string;
}

export interface AdditionalDetailsS {
    FirstUse:     string;
    Good:         string;
    Satisfactory: string;
    Construccion: string;
    Repaired:     string;
    Renovation:   string;
    ToBeRenewed:  string;
    Project:      string;
}

export interface BodyType {
    chassiscabine:      string;
    citadinodiesel:     string;
    citadinogasolina:   string;
    economico:          string;
    executivo:          string;
    executivotop:       string;
    familiar:           string;
    familiarpeqdiesel:  string;
    familiarpeqsw:      string;
    familiarpequeno:    string;
    familiarsw:         string;
    furgão:             string;
    furgãogrande:       string;
    furgãomedio:        string;
    furgoneta:          string;
    monovolume7l:       string;
    monovolume9l:       string;
    "pickup cab dupla": string;
    van:                string;
}

export interface BodyType2 {
    Truck:           string;
    "Pick-up":       string;
    Berlina:         string;
    Carrinha:        string;
    Coupe:           string;
    FaroisNevoeiro:  string;
    Furgao:          string;
    Citadino:        string;
    PequenoCitadino: string;
    Utilitario:      string;
    Sedan:           string;
    Monovolume:      string;
    SUV_TT:          string;
    Cabrio:          string;
}

export interface BuildingEnvironments {
    SecurityGate:       string;
    ElectricPowerPlant: string;
    WoodFloor:          string;
    Telefonica:         string;
    WaterTank:          string;
    Pool:               string;
    Alarm:              string;
    Playground:         string;
    Safe:               string;
    Elevator:           string;
    GatedCommunity:     string;
}

export interface Categories {
    House:             string;
    Apartment:         string;
    Home:              string;
    bedroom:           string;
    Ground:            string;
    Stores:            string;
    Office:            string;
    Warehouse:         string;
    FarmsAndHomestead: string;
    Garages:           string;
    Collection:        string;
    Building:          string;
    Transfer:          string;
}

export interface Color {
    white:  string;
    black:  string;
    red:    string;
    yellow: string;
    blue:   string;
    green:  string;
    orange: string;
    purple: string;
    grey?:  string;
    brown:  string;
    Grey?:  string;
}

export interface Conditions {
    new:         string;
    used:        string;
    consignment: string;
    service:     string;
}

export interface Countries {
    PORTUGAL: string;
    ESPAÑA:   string;
    USA:      string;
}

export interface EnvironmentsS {
    Terrace:     string;
    ServiceRoom: string;
    StudioRoom:  string;
    Pool:        string;
    Storage:     string;
    Box1:        string;
    Box2:        string;
    Parking1:    string;
    Parking2:    string;
    Suite:       string;
    Balcony:     string;
}

export interface EquipmentList {
    soundSystem:                       string;
    lightSensor:                       string;
    airbag:                            string;
    frontArmrest:                      string;
    seatBeltAlert:                     string;
    adjustableDriverSeat:              string;
    aluminiumRoofRails:                string;
    airConditioning:                   string;
    cb:                                string;
    leavingOrComingHomeFunction:       string;
    onBoardComputer:                   string;
    usbConnection:                     string;
    tyrePressureMonitoring:            string;
    cruiseControl:                     string;
    powerSteering:                     string;
    antiGlareInteriorMirror:           string;
    fogLamps:                          string;
    keyWithCommand:                    string;
    immobilizer:                       string;
    isofixSystem:                      string;
    alloyWheels:                       string;
    daytimeRunningLights:              string;
    technologyPackI:                   string;
    illuminatedVanityMirrors:          string;
    electricRearviewMirrors:           string;
    rainSensor:                        string;
    parkingAid:                        string;
    startStopSystem:                   string;
    "12vElectricalSocket":             string;
    electricParkingBrake:              string;
    electricWindows:                   string;
    leatherSteeringWheel:              string;
    multifunctionSteeringWheelPackage: string;
    multifunctionSteeringWheel:        string;
    steeringWheelAdjustment:           string;
    headRestraints:                    string;
    sportsSeats:                       string;
    bluetoothConnection:               string;
    radioWithTouchScreenBluetoothUsb:  string;
    radioWithBluetooth:                string;
    "4wheelDrive":                     string;
    hillStartAssistance:               string;
    smokingPack:                       string;
    packLightAndVision:                string;
    navigationSystem:                  string;
}

export interface FacilitiesS {
    EquippedKitchen:    string;
    Furnished:          string;
    CentralHeating:     string;
    AirConditioning:    string;
    Accessibility:      string;
    Garden:             string;
    Fireplace:          string;
    Pool:               string;
    ElectricPowerPlant: string;
    WoodFloor:          string;
}

export interface FloorS {
    Attic:       string;
    Basement:    string;
    GroundFloor: string;
}

export interface Fuel {
    gasoline:       string;
    diesel:         string;
    electric:       string;
    phev:           string;
    cng:            string;
    "gasoline/cng": string;
    eHybrid:        string;
    gasHybrid:      string;
    pluginHybrid:   string;
}

export interface Gearbox {
    manual:    string;
    automatic: string;
    dual:      string;
    other:     string;
}

export interface MetricUnit {
    km:    string;
    miles: string;
    other: string;
}

export interface OfferTypeS {
    Sell:        string;
    Rent:        string;
    SellAndRent: string;
    Bank:        string;
    Vacation:    string;
}

export interface OptionsList {
    fullMaintenance:     string;
    driverHelpline:      string;
    singleTax:           string;
    travelAssistance:    string;
    protectionNegligent: string;
    periodicInspection:  string;
}

export interface ServicesS {
    Electricity:        string;
    Water:              string;
    Wastewater:         string;
    Phone:              string;
    Gas:                string;
    UrbanTrash:         string;
    CableTV:            string;
    SatelliteTV:        string;
    FiberOpticTV:       string;
    InternetFiberOptic: string;
    Wifi:               string;
}

export interface Source {
    title:     string;
    priceList: string;
    eurolinea: string;
    autoline:  string;
    other:     string;
}

export interface TentacledStatus {
    sold:     string;
    Unsold?:  string;
    inactive: string;
    unsold?:  string;
}

export interface TentacledType {
    title:              string;
    new:                string;
    occasion:           string;
    potential_recovery: string;
}

export interface VehicleType {
    Type_1: string;
    Type_2: string;
    Type_3: string;
    Type_4: string;
}

export interface Vicinity {
    School:         string;
    Supermarket:    string;
    Pharmacy:       string;
    CommercialArea: string;
    Hospital:       string;
    Train:          string;
    Bus:            string;
    Taxi:           string;
    Airport:        string;
    mall:           string;
    Auditorium:     string;
    playground:     string;
    MovieTheater:   string;
    MusicAcademy:   string;
    Gymnasium:      string;
    Beach:          string;
    Montana:        string;
}

export interface ProductsList {
    title:         string;
    myTitle:       string;
    createButton:  string;
    categoria?:    string;
    urbanization:  string;
    bedrooms:      string;
    bathrooms:     string;
    parkingPlaces: string;
    area:          string;
    usdPrice:      string;
    category?:     string;
}

export interface Messages {
    synchronize:      string;
    synchronizeAlert: string;
    yes:              string;
    no:               string;
}

export interface Profile {
    title:                      string;
    level:                      string;
    title2:                     string;
    lastSession:                string;
    visits:                     string;
    title3:                     string;
    updated:                    string;
    contacts:                   string;
    totalPeople:                string;
    totalPeopleOwner:           string;
    totalPeopleAssigned:        string;
    products:                   string;
    totalProducts:              string;
    totalProductsManaged:       string;
    totalProductsPc:            string;
    totalProductsAssigned:      string;
    totalLinks:                 string;
    transactions:               string;
    totalTransactions:          string;
    totalTransactionsPcWon:     string;
    amountPcWon:                string;
    commissionPcWon:            string;
    activities:                 string;
    totalActivities:            string;
    totalActivitiesVisit:       string;
    totalActivitiesEmail:       string;
    totalActivitiesCall:        string;
    totalActivitiesEvent:       string;
    totalActivitiesUpdatePrice: string;
    totalActivitiesAssigned:    string;
    totalActivitiesOther:       string;
}

export interface Properties {
    form: PropertiesForm;
    list: NewClass;
}

export interface PropertiesForm {
    title1:                       string;
    propertyName:                 string;
    type:                         StickyType;
    trackingId:                   string;
    deafaultUrl:                  string;
    defaulView:                   string;
    viewId:                       string;
    industryCategory:             string;
    manualTagging:                string;
    title2:                       string;
    lastDay:                      string;
    last7Days:                    string;
    last30Days:                   string;
    title3:                       string;
    useEnhancedLinkAttribution:   string;
    enableUsersMetricInReporting: string;
    vendor:                       FluffyVendor;
}

export interface StickyType {
    name:       string;
    web:        string;
    androidApp: string;
    ioApp:      string;
    fanpage:    string;
    instagram:  string;
    whatsApp:   string;
    ads:        string;
    post:       string;
    otherApp:   string;
}

export interface FluffyVendor {
    name:     string;
    google:   string;
    facebook: string;
    twitter:  string;
    linkedIn: string;
    other:    string;
}

export interface Quotes {
    form: QuotesForm;
    list: QuotesList;
}

export interface QuotesForm {
    people:          string;
    subject:         string;
    transactionName: string;
    grandTotal:      string;
    shareLink:       string;
}

export interface QuotesList {
    title:            string;
    transactionName?: string;
}

export interface Reports {
    general: ReportsGeneral;
}

export interface ReportsGeneral {
    salesStock:        SalesOportunitiesClass;
    clients:           Clients;
    salesOportunities: SalesOportunitiesClass;
}

export interface Clients {
    title:   string;
    title2:  string;
    title3:  string;
    title4:  string;
    title5:  string;
    title6:  string;
    title7:  string;
    title8:  string;
    title9:  string;
    title10: string;
    title11: string;
}

export interface SalesOportunitiesClass {
    title:             string;
    title2:            string;
    title3:            string;
    title4:            string;
    title5:            string;
    title6:            string;
    title7:            string;
    title8:            string;
    title9:            string;
    title10:           string;
    title11:           string;
    title12:           string;
    title13:           string;
    title14?:          string;
    title15?:          string;
    title16?:          string;
    indicator?:        string;
    monthAverage?:     string;
    prevMonthAverage?: string;
    variation?:        string;
}

export interface Role {
    form: RoleForm;
    list: NewClass;
}

export interface RoleForm {
    actions:     FormActions;
    name:        string;
    description: string;
    modules:     string;
}

export interface FormActions {
    create:     string;
    edit:       string;
    read:       string;
    delete:     string;
    softDelete: string;
    undelete:   string;
}

export interface Roles {
    form: RolesForm;
    list: LeadsList;
    new:  NewClass;
    edit: NewClass;
}

export interface RolesForm {
    title: string;
    role:  string;
}

export interface Scheduler {
    title:          string;
    today:          string;
    allDay:         string;
    dayViewTitle:   string;
    weekViewTitle:  string;
    monthViewTitle: string;
    showWorkDay:    string;
    showFullDay:    string;
}

export interface Segments {
    form: SegmentsForm;
    list: NewClass;
}

export interface SegmentsForm {
    title:       string;
    title1:      string;
    description: string;
    title2?:     string;
    filter?:     string;
    title3?:     string;
}

export interface TranslationSettings {
    title:        string;
    contactsType: string;
}

export interface Sidebar {
    dashboard:       UsersClass;
    users:           UsersClass;
    offices:         AllPeopleClass;
    companies?:      AllPeopleClass;
    leads:           AllPeopleClass;
    contacts:        AllPeopleClass;
    allPeople:       AllPeopleClass;
    people:          AllPeopleClass;
    peoples:         AllPeopleClass;
    employees:       AllPeopleClass;
    clients:         AllPeopleClass;
    organizations:   AllPeopleClass;
    dealers:         AllPeopleClass;
    products:        AllPeopleClass;
    quotes:          AllPeopleClass;
    transactions:    AllPeopleClass;
    campaigns:       AllPeopleClass;
    activities:      AllPeopleClass;
    cases:           AllPeopleClass;
    files:           AllPeopleClass;
    settings:        SidebarSettings;
    "contact-lists": AllPeopleClass;
    segments:        AllPeopleClass;
    templates:       AllPeopleClass;
    communications:  AllPeopleClass;
    properties:      AllPeopleClass;
    "web-analytics": AllPeopleClass;
    role:            AllPeopleClass;
    workflow:        AllPeopleClass;
    roles:           AllPeopleClass;
    reports:         AllPeopleClass;
    logout:          string;
}

export interface AllPeopleClass {
    title:         string;
    add:           Add;
    myList?:       string;
    next?:         string;
    list:          Topic;
    teamSkyGroup?: string;
    sidebarTitle?: string;
    listTitle?:    string;
}

export enum Add {
    Add = "Add",
    Adicionar = "Adicionar",
    Agregar = "Agregar",
    Añadir = "Añadir",
}

export interface UsersClass {
    title:    string;
    title1?:  string;
    title2?:  string;
    title3?:  string;
    add:      Add;
    list:     Topic;
    licenses: string;
    myList?:  string;
}

export interface SidebarSettings {
    title:         string;
    subtitle:      string;
    modulesConfig: string;
}

export interface Transactions {
    form:    TransactionsForm;
    list:    TransactionsList;
    new:     NewClass;
    edit:    NewClass;
    message: TransactionsMessage;
}

export interface TransactionsForm {
    subject:                       string;
    title2:                        string;
    title3:                        string;
    title4:                        string;
    title5:                        string;
    title6:                        string;
    title7:                        string;
    title8:                        string;
    saleAmount:                    string;
    dealershipPct:                 string;
    officeName?:                   string;
    dealershipAmount:              string;
    seller?:                       string;
    sellerPct:                     string;
    sellerAmount:                  string;
    commissionableAmount:          string;
    commissionableAmountFinancing: string;
    totalCommissionable?:          string;
    type:                          string;
    types:                         FluffyTypes;
    status?:                       string;
    statusSelect?:                 StatusSelect;
    code:                          string;
    price:                         string;
    product:                       string;
    currency:                      string;
    date:                          string;
    amount:                        string;
    amount2:                       string;
    estimatedClosingDate:          string;
    transactionDate:               string;
    acquisitionDate:               string;
    sellerName:                    string;
    lastSellerName:                string;
    buyerName:                     string;
    commissionPercentage:          string;
    commissionTotal:               string;
    commissionRoyalty:             string;
    assessor:                      string;
    invoiceAmountSeller:           string;
    invoiceAmountBuyer:            string;
    offerAmount:                   string;
    offerDate:                     string;
    reservationAmount:             string;
    reservationDate:               string;
    promisePay:                    string;
    promisePayDate:                string;
    promiseAmount:                 string;
    promiseDate:                   string;
    commissionPartial:             string;
    commissionDate:                string;
    protocolizationPay:            string;
    protocolizationPayDate:        string;
    protocolizationAmount:         string;
    protocolizationDate:           string;
    AmountPaid:                    string;
    AmountPending:                 string;
    documentLink:                  string;
    lawyerName:                    string;
    requirements:                  string;
    billingAddress:                string;
    shippingAddress?:              string;
    sellerAdvisor:                 string;
    advisor:                       string;
    registrationCode:              string;
    amountPhase:                   string;
    phase:                         string;
    otherData:                     string;
    financingType:                 FinancingType;
    typeCommission:                TypeCommission;
    commission:                    string;
    services:                      OptionsList;
    optionalServices:              OptionalServices;
    damageInsurance:               DamageInsurance;
    contractedTires:               ContractedTires;
    maintenance:                   Maintenance;
    extendedWarranty:              ExtendedWarranty;
    carInsurance:                  CarInsurance;
    ownDamage:                     OwnDamage;
    replacementCar:                ReplacementCar;
    deadlineContract:              string;
    monthlyFee:                    string;
    initial:                       string;
    atEndContract:                 AtEndContract;
    statuses:                      Statuses;
    plan:                          Plan;
    salesPhase:                    MaintenancePhaseClass;
    reasonLostName?:               string;
    reasonLost:                    ReasonLost;
    rentingPhase:                  OpenOptionPhaseClass;
    openOptionPhase:               OpenOptionPhaseClass;
    servicePhase:                  ServicePhase;
    maintenancePhase:              MaintenancePhaseClass;
    lawyer_requirements?:          LawyerRequirements;
    totalommissionable?:           string;
}

export interface AtEndContract {
    title:      string;
    toExchange: string;
    refinance:  string;
    giveBack:   string;
    pay:        string;
}

export interface CarInsurance {
    title:                string;
    franchise:            string;
    liabilityInsurance:   LiabilityInsurance;
    occupants:            Occupants;
    deadlineCarInsurance: string;
}

export interface LiabilityInsurance {
    title:     string;
    mandatory: string;
    "50M":     string;
}

export interface Occupants {
    title: string;
    other: string;
}

export interface ContractedTires {
    title:                 string;
    title2:                string;
    unlimited:             string;
    limited:               string;
    quantity:              string;
    deadlineContractTyres: string;
}

export interface DamageInsurance {
    "500":                   string;
    title:                   string;
    title2:                  string;
    other:                   string;
    deadlineDamageInsurance: string;
    quantityDamageInsurance: string;
}

export interface ExtendedWarranty {
    title?:                 string;
    title2?:                string;
    warrantyExtensionStart: string;
    warrantyExtensionEnds:  string;
    kmsWarrantyExtension:   string;
}

export interface FinancingType {
    financing:       string;
    renting:         string;
    autoCredit:      string;
    leasing:         string;
    credit:          string;
    ald:             string;
    otherFinancing:  string;
    clientFinancing: string;
    instantPayment:  string;
}

export interface LawyerRequirements {
    docProperty:              string;
    photocopyOfCIOwner:       string;
    photocopyOfCIbuyer:       string;
    photocopyOfTheSalesCheck: string;
    cadastralCertificate:     string;
    solvencies:               string;
    realEstateTransaction:    string;
    form33Seniat:             string;
    mainHousing:              string;
}

export interface Maintenance {
    title:                       string;
    title2:                      string;
    basic:                       string;
    full:                        string;
    quantityMaintenance:         string;
    deadlineContractMaintenance: string;
}

export interface MaintenancePhaseClass {
    meetGreet:                string;
    showroomVisit:            string;
    quote:                    string;
    negotiationReview:        string;
    offer:                    string;
    type?:                    string;
    contracts:                string;
    firm:                     string;
    firstMonthPay?:           string;
    delivery:                 string;
    V00_leads:                string;
    V01_diagnostico:          string;
    V02_testDrive:            string;
    V03_proposta:             string;
    V04_fechoDoContrato:      string;
    V05_financiamento_seguro: string;
    V06_preparacaoDaEntrega:  string;
    V30_followup?:            string;
    V200_lost?:               string;
    V20_delayed?:             string;
    financing?:               string;
    initialPay?:              string;
    finalPay?:                string;
    V07_delivery?:            string;
}

export interface OpenOptionPhaseClass {
    meetGreet:         string;
    showroomVisit:     string;
    quote:             string;
    negotiationReview: string;
    offer:             string;
    type:              string;
    contracts:         string;
    firm:              string;
    firstMonthPay:     string;
    delivery:          string;
}

export interface OptionalServices {
    title:            string;
    extendedWarranty: string;
    maintenance:      string;
    damageInsurance:  string;
    tires:            string;
    carInsurance:     string;
    ownDamage:        string;
    replacementCar:   string;
}

export interface OwnDamage {
    title:                           string;
    CrashCollisionRollover:          string;
    FireLightningExplosion:          string;
    theftRobbery:                    string;
    SocialRisksNaturePhenomena:      string;
    travelAssistanceLegalProtection: string;
    replacementCar:                  string;
}

export interface Plan {
    title: string;
    plan1: string;
    plan2: string;
    plan3: string;
    plan4: string;
    plan5: string;
}

export interface ReasonLost {
    name:                         string;
    V21_lostBrandUsed:            string;
    V22_lostBrandFinancing:       string;
    V23_lostBrandPrice:           string;
    V24_lostOthersBrandUsed:      string;
    V25_lostOthersBrandFinancing: string;
    V26_lostOthersBrandPrice:     string;
    V27_lostOthers:               string;
}

export interface ReplacementCar {
    title:                           string;
    daysFailure:                     Days;
    daysIncident:                    Days;
    daysTheft:                       ContactsClass;
    FireLightningExplosion:          string;
    theftRobbery:                    string;
    SocialRisksNaturePhenomena:      string;
    travelAssistanceLegalProtection: string;
    replacementCar:                  string;
}

export interface Days {
    title:     string;
    title2:    string;
    unlimited: string;
    limited:   string;
}

export interface ServicePhase {
    meetGreet:         string;
    appointment:       string;
    quote:             string;
    negotiationReview: string;
    execution:         string;
    payment:           string;
    delivery:          string;
}

export interface StatusSelect {
    open:  string;
    won:   string;
    close: string;
}

export interface Statuses {
    offer:            string;
    reservation:      string;
    bilateralPromise: string;
    protocolization:  string;
}

export interface TypeCommission {
    title:        string;
    percentage:   string;
    noCommission: string;
    fixed:        string;
}

export interface FluffyTypes {
    financing:   string;
    sale:        string;
    rental:      string;
    other:       string;
    renting:     string;
    openOptions: string;
    service:     string;
    replacement: string;
    maintenance: string;
    insurance:   string;
    AUTO:        string;
}

export interface TransactionsList {
    add:          string;
    title:        string;
    myTitle:      string;
    createButton: string;
    price:        string;
}

export interface TransactionsMessage {
    fieldsError:        string;
    success:            string;
    editSuccess:        string;
    requestError:       string;
    fieldAlreadyExists: string;
    fieldAvailable:     string;
    tryAgainLater:      string;
    exit:               string;
    continueCreate:     string;
    continue:           string;
    BottomDuplicate:    string;
    duplicate:          string;
}

export interface Users {
    form:     UsersForm;
    list:     UsersList;
    new:      UsersNew;
    edit:     UsersEdit;
    licenses: Licenses;
}

export interface UsersEdit {
    title:              string;
    cloneButtom:        string;
    deleteButtom:       string;
    softDeleteButtom:   string;
    undeleteButtom:     string;
    saveButtom:         string;
    saveExitButtom:     string;
    converTo:           string;
    sendCommunications: string;
    Email:              string;
    sendEmail:          string;
}

export interface UsersForm {
    title:           string;
    title1?:         string;
    name:            string;
    lastname:        string;
    stage:           string;
    phone:           string;
    email:           string;
    secondaryPhone:  string;
    thirdPhone:      string;
    fourthPhone:     string;
    secondaryEmail:  string;
    level:           string;
    cOffice:         string;
    contact:         string;
    userParent:      string;
    parentUserName:  string;
    password:        string;
    info:            string;
    creationDate:    string;
    updateDate:      string;
    filter:          string;
    userName:        string;
    lastUserName:    string;
    userDeletedName: string;
    deletedAt:       string;
    keyloopId?:      string;
    levels:          Levels;
}

export interface Levels {
    administrator?:   string;
    office:           string;
    general:          string;
    salesperson:      string;
    callCenterAgent:  string;
    insuranceManager: string;
    financeManager:   string;
    usedManager:      string;
    Administrator?:   string;
}

export interface Licenses {
    title:            string;
    manageAccount:    string;
    licenses:         string;
    active:           string;
    free:             string;
    nextPayment:      string;
    lastPayment:      string;
    frequency:        string;
    visits:           string;
    lastMonth:        string;
    currentMonth:     string;
    soon:             string;
    acquiredVsActive: string;
    visitsVsUsers:    string;
    User:             string;
    Visits:           string;
    LastVisit:        string;
    Enero:            string;
    Febrero:          string;
    Marzo:            string;
    Abril:            string;
    Mayo:             string;
    Junio:            string;
    Julio:            string;
    Agosto:           string;
    Septiembre:       string;
    Octubre:          string;
    Noviembre:        string;
    Diciembre:        string;
}

export interface UsersList {
    title:                       string;
    add:                         string;
    total:                       string;
    details:                     string;
    records:                     string;
    search:                      string;
    noRecords:                   string;
    filterStartsWithOperator:    string;
    filterEndsWithOperator:      string;
    filterContainsOperator:      string;
    filterBeforeOrEqualOperator: string;
    filterBeforeOperator:        string;
    filterAfterOrEqualOperator:  string;
    filterAfterOperator:         string;
    filterNotEqOperator:         string;
    filterNotContainsOperator:   string;
    filterLteOperator:           string;
    filterLtOperator:            string;
    filterIsTrue:                string;
    filterIsFalse:               string;
    filterIsNullOperator:        string;
    filterIsNotNullOperator:     string;
    filterIsNotEmptyOperator:    string;
    filterIsEmptyOperator:       string;
    filterGteOperator:           string;
    filterGtOperator:            string;
    filterEqOperator:            string;
}

export interface UsersNew {
    title:             string;
    moreAdvisorButtom: string;
    createButtom:      string;
    createExitButtom:  string;
}

export interface WebAnalytics {
    form: WebAnalyticsForm;
    list: NewClass;
}

export interface WebAnalyticsForm {
    active1DayUsers:                     string;
    active28DayUsers:                    string;
    active7DayUsers:                     string;
    activeUsers:                         string;
    adUnitExposure:                      string;
    addToCarts:                          string;
    averagePurchaseRevenue:              string;
    averagePurchaseRevenuePerPayingUser: string;
    averageRevenuePerUser:               string;
    cartToViewRate:                      string;
    checkouts:                           string;
    cohortActiveUsers:                   string;
    cohortTotalUsers:                    string;
    conversions:                         string;
    ecommercePurchases:                  string;
    engagedSessions:                     string;
    engagementRate:                      string;
    eventCount:                          string;
    eventCountPerUser:                   string;
    eventValue:                          string;
    eventsPerSession:                    string;
    firstTimePurchasers:                 string;
    itemListClickThroughRate:            string;
    itemListClicks:                      string;
    itemListViews:                       string;
    itemPromotionClickThroughRate:       string;
    itemPromotionClicks:                 string;
    itemPromotionViews:                  string;
    itemPurchaseQuantity:                string;
    itemRevenue:                         string;
    itemViews:                           string;
    newUsers:                            string;
    publisherAdClicks:                   string;
    publisherAdImpressions:              string;
    purchaseRevenue:                     string;
    purchaseToViewRate:                  string;
    screenPageViews:                     string;
    sessions:                            string;
    sessionsPerUser:                     string;
    totalAdRevenue:                      string;
    totalPurchasers:                     string;
    totalRevenue:                        string;
    totalUsers:                          string;
    transactions:                        string;
    userEngagementDuration:              string;
}
