import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service'

@Injectable()
export class UploadFileService {
    public cloneData: any = {}
    public data: any = {}
    constructor(
        private apiService: ApiService,
        private configService: ConfigService,
    ) { }

    post(body: object, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.post(body, _module, this.getHeader())
    }

    postToN8N(body: object, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.postToN8N(body, _module)
    }

    get(controller?: string, id?: string, params?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.get(_module, id, params)
    }

    delete(controller?: string, id?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.delete(_module, id)
    }

    update(body: any, id: string, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.put(body, _module, id)
    }

    patch(body: any, id: string, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.patch(body, _module, id)
    }
    
    getHeader() {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Access-Control-Allow-Origin', '*');
        return headers
    }
}