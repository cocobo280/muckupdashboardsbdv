import { Injectable, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { apiEndPoint } from "../../shared/utilities/api-url";
// import { TranslateService } from "@ngx-translate/core";
// import { TranslateService } from "./translate.service";

declare let Messenger: any;
type Token = { accessToken: string, refreshToken: string, exp: Date, id: string }

@Injectable()
export class AuthService {

    public token: Token;
    public user: any;
    private apiEndPoint: any = ''//apiEndPoint.url;
    public userInfoChange: EventEmitter<any> = new EventEmitter();
    readonly modules = {
        "Lead": "leads",
        "People": "peoples",
        "Employee": "employees",
        "Activity": "activities",
        "Case": "cases",
        "Company": "companies",
        "Office": "offices",
        "Product": "products",
        "Quote": "quotes",
        "Transaction": "transactions",
        "Campaign": "campaigns",
        "Segmentation": "segments",
        "ContactList": "contact-lists",
        "Template": "templates",
        "Communication": "communications",
        "User": "users",
        "Property": "properties",
        "WebAnalytics": "web-analytics",
        "ListFilter": "list-filters",
        "Config": "config"
    }

    // readonly roles = {
    //     "/analytics": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/reports/general": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/leads/create": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/leads/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/peoples/create": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/peoples/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/communications/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/activities/create": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/activities/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/cases/create": ["administrator", "office", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/cases/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/offices/create": ["administrator"],
    //     "/offices/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/transactions/create": ["administrator", "general", "salesperson"],
    //     "/transactions/list": ["administrator", "general", "salesperson"],
    //     "/products/create": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/products/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/quotes/create": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/quotes/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/campaigns/create": ["administrator"],
    //     "/campaigns/list": ["administrator"],
    //     "/segments/create": ["administrator"],
    //     "/segments/list": ["administrator"],
    //     "/contact-lists/create": ["administrator"],
    //     "/contact-lists/list": ["administrator"],
    //     "/templates/create": ["administrator"],
    //     "/templates/list": ["administrator"],
    //     "/profile": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/companies/create": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/companies/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/employees/create": ["administrator"],
    //     "/employees/list": ["administrator", "office", "general", "salesperson", "callCenterAgent", "insuranceManager", "financeManager", "usedManager"],
    //     "/users/create": ["administrator"],
    //     "/users/list": ["administrator", "general"],
    // }

    constructor(
        private router: Router,
        private http: HttpClient
    ) { }

    setKickstart(data: any) {
        localStorage.setItem("endPoints", JSON.stringify(data.endPoints));
        localStorage.setItem("kickstart", JSON.stringify(data.kickstart));
        this.apiEndPoint = data.endPoints.url;
    }

    /**Devuelve valores por defecto */
    getKickStart(uri) {
        return this.http.get(uri)
    };

    get kickStart() {
        const kickstart = localStorage.getItem("kickstart");
        return kickstart ? JSON.parse(kickstart) : {};
    }

    login(body) {
        return this.http.post('https://app.sidis.ai/caferoyal/api/' + 'sessions', body)
    }

    refreshToken(body, id) {
        return this.http.patch(this.apiEndPoint + 'sessions/' + id, body)
    }

    async setAuthData(data: any): Promise<boolean> {
        let parseData = this.parseJwt(data.accessToken);
        if (parseData.role.name) {
            parseData.level = parseData.role.name
            parseData.modules = this.convertSubject(parseData.role.permissions);
            this.setUser(parseData)
            this.setToken({ accessToken: data.accessToken, refreshToken: data.refreshToken, exp: this.getExpireDate(parseData.exp, parseData.iat), id: data.id });
            return true
        } else {
            this.logout();
            return false
        }
    }

    setToken(token: Token) {
        this.token = token;
        localStorage.setItem('token', JSON.stringify(token))
    }

    setUser(user: any) {
        this.user = user;
        localStorage.setItem('user', JSON.stringify(user))
        this.userInfoChange.emit(this.getUser())
    }

    getToken(): Token {
        this.token = this.token ? this.token : JSON.parse(localStorage.getItem('token'))
        return this.token;
    }

    getUser() {
        this.user = this.user ? this.user : JSON.parse(localStorage.getItem('user'))
        return this.user
    }

    checkToken(): boolean {
        let checked = this.token ? true : localStorage.getItem('token') ? true : false
        return checked
    }

    logout() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }

    isAdmin(): boolean {
        return this.getUser().level == 'administrator'
    }

    isAgent(): boolean {
        return this.getUser().level == 'agent'
    }

    isFranchise(): boolean {
        return this.getUser().level == 'franchise'
    }

    checkEmailCredentials(withOutMessage?: boolean): boolean {
        // if (!this.user.credentials) {
        //     if (!withOutMessage) {
        //         let msg = Messenger({ extraClasses: 'messenger-fixed messenger-on-top' }).post({
        //             message: this.translateService.instant('inbox.noCredentials'),
        //             id: "Only-one-message",
        //             type: 'error',
        //             hideAfter: 100,
        //             actions: {
        //                 retry: {
        //                     label: this.translateService.instant('inbox.setNow'),
        //                     action: function () {
        //                         this.router.navigate(['/data/sources/email'])
        //                         msg.hide()
        //                     }.bind(this)
        //                 },
        //                 cancel: {
        //                     label: this.translateService.instant('inbox.setLater'),
        //                     action: function () {
        //                         msg.hide()
        //                     }
        //                 }
        //             }
        //         })
        //     }            
        //     return false
        // }else{
        //     return true
        // }
        return true
    }

    checkAuth(module: string, action: string): boolean {
        if (this.getUser().modules[module]) {
            return this.getUser().modules[module][action] ? this.getUser().modules[module][action] : false
        }
        return false
    }

    getHeader() {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Access-Control-Allow-Origin', '*');
        headers = headers.append('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,POST,DELETE,OPTIONS');
        headers = headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers = headers.append('Content-Type', 'application/json');

        return headers
    }

    parseJwt(token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        return JSON.parse(jsonPayload);
    };

    getExpireDate(exp: number, iat: number): Date {
        let now = Date.now()
        return new Date(now + ((exp - iat) * 1000))
    }

    convertSubject(permissions: any[]) {
        let manage = { read: true, create: true, delete: true, update: true, softDelete: true, unDelete: true }
        let res: any = {};
        permissions.forEach(crr => {
            if (!Array.isArray(crr.subject)) crr.subject = [crr.subject];
            if (!Array.isArray(crr.action)) crr.action = [crr.action];
            crr.action.map(action => {
                crr.subject.forEach(subject => {
                    res[this.modules[subject]] ? res[this.modules[subject]] = { ...res[this.modules[subject]], ...(action === 'manage' ? manage : { [action]: !crr.inverted }) } : res[this.modules[subject]] = { ...(action === 'manage' ? manage : { [action]: !crr.inverted }) }
                })
            }).reduce((t, x) => ({ ...t, ...x }), {})
        })
        return res

    };

    public get modulesList(): Object {
        return this.modules
    }
}
