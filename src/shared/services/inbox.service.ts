import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { AlertService } from './alert.service';
import { ConfigService } from "./config.service";

@Injectable()

export class InboxService {

  private emails: Object[] = [];
  private phoneNumbers: Object[] = [];
  private hasInfo: boolean = false;
  public hasNewInfo: EventEmitter<boolean> = new EventEmitter();
  public send: EventEmitter<boolean> = new EventEmitter();
  public eLinkChange: EventEmitter<string> = new EventEmitter();
  public relationId: string | string[] = '';

  eLink: string = '';
  readonly modules = {
    "leads": "leadId",
    "peoples": "peopleId",
    "employees": "employeeId",
    "activities": "activityId",
    "cases": "caseId",
    "companies": "companyId",
    "offices": "officeId",
    "products": "productId",
    "quotes": "quoteId",
    "transactions": "transactionId",
    "campaigns": "campaignId",
    "segments": "segmentationId",
    "contact-lists": "contactListId",
    "actions": "actionId",
    "templates": "templateId",
    "communications": "communicationId",
    "users": "userId",
    "properties": "propertyId",
    "web-analytics": "webAnalyticsId",
    "list-filters": "listFilterId",
    "shipping-rates": "shippingRateId",
    "coupons": "couponId",
    "tax-rates": "taxRateId",
    "payments": "paymentId",
    "subscriptions": "subscriptionId",
    "invoices": "invoiceId",
    "payment-links": "paymentLinkId",
    "utm-link-builders": "utmLinkBuilderId"
  }

  constructor(
    private apiService: ApiService,
    private alertService: AlertService,
    private authService: AuthService,
    private configService: ConfigService
  ) { }

  setInfo(data: { email: string, phone: string }[], field: 'email' | 'phone', wichOne: 'emails' | 'phoneNumbers') {
    this[wichOne] = data.filter(el => el[field])
    if (this[wichOne].length == 0) {
      this.alertService.alert('No contact information found', 'info')
    } else {
      this.hasInfo = true;
      this.hasNewInfo.emit(true);
    }
  }

  getInfo(wichOne: 'emails' | 'phoneNumbers') {
    if (this.hasInfo) {
      return this[wichOne]
    }
  }

  setRelationId(id: string | string[]) {
    this.relationId = id;
  }

  getRelationId(): string | string[] {
    return this.relationId
  }

  clearInfo() {
    this.emails = [];
    this.phoneNumbers = [];
    this.hasInfo = false;
  }

  parseEmailMessage(data: any[], subject: string, contentBody: string, contentType: "text/plain" | "text/html" | "text/mjml", relationId?: string) {
    let modl = this.configService.getCurrentModule();
    let exId = this.configService.getFieldsOfForm(modl).filter(el => el.emailInfo)[0]
    let body = [];
    let user = this.authService.getUser().personId;
    data.forEach(el => {
      body.push({
        fromId: user,
        fromEmail: user,
        toId: exId.name ? el[exId.name] : modl == 'peoples' ? el.id : '',
        toEmail: el.email ? el.email : '',
        state: "inQueue",
        subject,
        content: {
          [contentType]: contentBody
        },
        module: modl,
        [this.modules[modl]]: el.id
      })
    })
    return body
  }
  // toId: el.email ? el.email : el.secondaryEmail ? el.secondaryEmail : null,
  // toId: el.phone ? el.phone : el.secondaryPhone ? el.secondaryPhone : null,
  parseSMSMessage(data: any[], contentBody: string, relationId?: string) {
    let modl = this.configService.getCurrentModule();
    let body = [];
    let user = this.authService.getUser().personId;
    data.forEach(el => {
      body.push({
        fromId: user,
        toId: el.id,
        content: {
          "text/plain": contentBody
        },
        module: modl,
        moduleElementId: this.relationId ? this.relationId : el.toId
      })
    })
    return body
  }

  sendMessagge(data: any[], type: "emails" | "text-messages") {
    this.apiService.post(data, 'communications/' + type).subscribe(res => {
      if (type == "emails") this.alertService.alert("inbox.emailSuccess", "success")
      if (type == "text-messages") this.alertService.alert("inbox.textMessagesSuccess", "success")
      this.relationId = '';
      this.send.emit(true);
    }, error => {
      if (type == "emails") this.alertService.alert("inbox.emailError", "error", null, null, error)
      if (type == "text-messages") this.alertService.alert("inbox.textMessagesError", "error", null, null, error)
      this.relationId = '';
      this.send.emit(false);
    })
  }

  set externalLink(link) {
    if (link) {
      this.eLink = link;
      this.eLinkChange.emit(link);
    }
  }
}