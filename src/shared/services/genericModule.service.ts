import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { ConfigService } from './config.service'

@Injectable()
export class GenericModuleService {

    public cloneData: any = {};
    public data: any = {};
    public tableStates: any = {};
    public saveTemplate: EventEmitter<boolean> = new EventEmitter();
    public loadTemplate: EventEmitter<any> = new EventEmitter();

    constructor(
        private apiService: ApiService,
        private configService: ConfigService,
        private authService: AuthService,
    ) { }

    post(body: object, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.post(body, _module)
    }

    get(controller?: string, id?: string, params?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.get(_module, id, params)
    }

    fullget(controller?: string, id?: string, params?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.fullget(_module, id, params)
    }

    postFromN8N(body: object, controller?: string) {
        return this.apiService.postFromN8N(body, controller)
    }

    getFromN8N(controller?: string, id?: string, params?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.getFromN8N(_module, id, params)
    }

    getPivot(controller: string) {
        return this.apiService.getPivot(controller)
    }

    delete(controller?: string, id?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.delete(_module, id)
    }

    update(body: any, id: string, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.put(body, _module, id)
    }

    patch(body: any, id: string, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.patch(body, _module, id)
    }

    put(body: any, id: string, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.put(body, _module, id)
    }

    head(filter: string, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.head(_module, filter)
    }

    setCloneData(data: any) {
        if (data) {
            delete data.id;
            delete data.history;
            delete data.offer;
            delete data.reservation;
            delete data.promise;
            delete data.protocolization;
            delete data.transactionDate;
            delete data.stage;
            delete data.amountPaid;
            delete data.amountPending;
            delete data.createdAt;
            delete data.updatedAt;
            delete data.active;
            delete data.stage;
            delete data.code;
            delete data.leadDate;
            delete data.convertedAt;
            delete data.clientDate;
            delete data.contactedType;
            delete data.nextContactDate;
            delete data.contactingDate;
            delete data.commentsNextContact;
            delete data.comments;
            delete data.contactPreference;
            delete data.hourContact;
            delete data.interests;
            delete data.othersNotes;
            delete data.haveCar;
            delete data.satisfaction;
            delete data.dateOfChange;
            delete data.reason;
            delete data.lastTransactionId;
            delete data.lastTransactionName;
            delete data.lastTransactionDate;
            delete data.lastProductId;
            delete data.lastProductName;
            delete data.lastSellerId;
            delete data.lastSellerName;
            delete data.lastQuoteId;
            delete data.lastQuoteName;
            delete data.lastEstimatedClosingDate;
            delete data.lastQuoteDate;
            let fields = this.configService.getFieldsOfForm().reduce((acc, el) => (el.name && el.name != "" ? { ...acc, [el.name]: el } : { ...acc }), {});
            Object.keys(data).forEach(field => { if (!field || !this.verifyClone(fields[field])) delete data[field]; });
            this.cloneData = data;
        } else {
            this.cloneData = null;
        }
    }

    verifyClone(config: { clonable: { field?: string, value?: string | string[], or?: { field?: string, value?: string | string[] }[], and?: { field?: string, value?: string | string[] }[] } | boolean }): boolean {
        if (config) {
            if (typeof config.clonable === 'boolean') return config.clonable;
            if (!config.clonable) return true;
            if (typeof config.clonable == 'object' && (config.clonable.and || config.clonable.or)) {
                let res: boolean
                if (config.clonable.and) {
                    config.clonable.and.forEach(el => {
                        if (!this.verifyClone({ clonable: el })) res = false;
                    })
                    return res != false
                }
                if (config.clonable.or) {
                    config.clonable.or.forEach(el => {
                        if (this.verifyClone({ clonable: el })) res = true;
                    })
                    return res == true
                }
            }
            if (typeof config.clonable == 'object' && config.clonable.field && config.clonable.value) {
                let user = this.authService.getUser();
                return config.clonable.value.indexOf(user[config.clonable.field]) != -1
            }
        }
        return true
    }


    getCloneData() {
        return this.cloneData ? this.cloneData : null
    }

    isPeople() {
        let mdl = this.configService.getCurrentModule()
        if (mdl == 'contacts' || mdl == 'leads') {
            return true
        }
    }

    isClient() {
        let mdl = this.configService.getCurrentModule()
        if (mdl == 'customers') {
            return true
        }
    }

    isTransactions() {
        let mdl = this.configService.getCurrentModule()
        if (mdl == 'transactions') {
            return true
        }
    }

    isProduct() {
        let mdl = this.configService.getCurrentModule()
        if (mdl == 'products') {
            return true
        }
    }

    setData(key: string, data: any, start?: number, list?: boolean) {
        if (list) {
            this.data[key] ? null : this.data[key] = [];
            this.data[key].splice(start, data.length, ...data);

        } else {
            this.data[key] = data;
        }
    }

    deleteFromData(key: string, id) {
        this.data[key] = this.data[key].filter(el => el.id != id);
    }

    setFilteredData(key: string, data: any) {
        this.data["filter" + key] = data;
        //this.saveOnLocalStorage("filter" + key, data)
    }

    setItemById(id, data, key?: string) {
        if (this.data[key]) {
            let item = this.data[key].find((el, index) => {
                if (el.id == id) {
                    this.data[key][index] = data
                }
                return el.id == id
            })
            item ? null : this.data[key].push(data)

        } else {
            let array = []
            array.push(data)
            this.data[key] = array;
        }
    }

    getItemById(id, key?: string) {
        let item
        if (this.data[key]) {
            item = this.data[key].find(el => {
                return el.id == id
            })
        }
        return item
    }

    getData(key: string) {
        return this.data[key] ? this.data[key] : []
        //return this.data[key] ? this.data[key] : this.getFromLocalStorage(key) ? this.getFromLocalStorage(key) : []
    }

    getFilteredData(key: string) {
        return this.data["filter" + key] ? this.data["filter" + key] : []
        //return this.data["filter" + key] ? this.data["filter" + key] : this.getFromLocalStorage("filter" + key) ? this.getFromLocalStorage("filter" + key) : []
    }

    saveOnLocalStorage(key: string, data: any) {
        localStorage.setItem(key, JSON.stringify(data))
    }

    getFromLocalStorage(key: string) {
        return JSON.parse(localStorage.getItem(key)) || null
    }

    emitSaveTemplate() {
        this.saveTemplate.emit(true);
    }

    emitTemplate(template: any) {
        this.loadTemplate.emit(template);
    }

    getModulesOfRelations() {
        const relationConfig = this.configService.getModuleConfig().relations
        return Object.keys(relationConfig).map(el => (relationConfig[el].module))
    }

    onSelectRelation(module: string, relation: string, data: any, notClone?: boolean) {
        data.name = data.lastName ? data.name ? data.name + " " + data.lastName : data.lastName : data.lastName
        const config: any = this.configService.getFieldsOfForm(relation).filter(el => el.lookup ? el.lookup.name == module : false)
        if (notClone) return config
        let result = {}
        config.forEach(el => {
            result = { ...result, ...el.lookup.relatedTo.child.reduce((acc, el2, index) => ({ ...acc, [el.lookup.relatedTo.parent[index]]: data[el2] }), {}) }
        });
        this.setCloneData(result)
    }

    fetchData(config: any, data: any) {
        data.name = data.lastName ? data.name ? data.name + " " + data.lastName : data.lastName : data.lastName
        let result = {}
        config.forEach(el => {
            result = { ...result, ...el.lookup.relatedTo.child.reduce((acc, el2, index) => ({ ...acc, [el.lookup.relatedTo.parent[index]]: data[el2] }), {}) }
        });
        return result
    }

    clearData() {
        this.data = {};
    }

    verifyTableState(state: any, modl?: string) {
        if (state) {
            let fieldTypes = this.configService.getTypesOfFieldsOfForm(modl)
            state.filters.forEach(el => {
                if (!el.filters) {
                    if (fieldTypes[el.field] == "date") el.value = new Date(el.value);
                }
                if (el.filters) this.verifyTableState(el)
            });
        }
        return state
    }
}