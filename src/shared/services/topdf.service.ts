import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { canvas } from 'leaflet';

@Injectable({
  providedIn: 'root'
})
export class TopdfService {
  constructor() { }

  toPDF(html: any, name: string) {
    let doc = new jsPDF('p','mm','letter');
    html2canvas(html).then(canvas => {
      document.getElementsByTagName('BODY')[0].removeChild(html)            
      let contentUrl = canvas.toDataURL('image/png')
      doc.addImage(contentUrl,'PNG',0,0)
      doc.save(name)    
    })
  }
}
