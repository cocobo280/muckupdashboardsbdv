import { HttpErrorResponse } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
declare let Messenger: any;

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  public alertResponse: EventEmitter<boolean> = new EventEmitter();
  constructor(private translate: TranslateService) { }

  alert(message: string, type: 'error' | 'success' | 'info', retry?: string, cancel?: string, error?: HttpErrorResponse) {
    Messenger().hideAll();
    if (!error) {
      if (retry && cancel) {
        let msg = Messenger({ extraClasses: 'messenger-fixed messenger-on-top' }).post({
          message: this.translate.instant(message),
          id: "Only-one-message",
          type: type,
          hideAfter: 100,
          actions: {
            retry: {
              label: this.translate.instant(retry),
              action: function () {
                this.alertResponse.emit(true)
                msg.hide()
              }.bind(this)
            },
            cancel: {
              label: this.translate.instant(cancel),
              action: function () {
                this.alertResponse.emit(false)
                msg.hide()
              }.bind(this)
            }
          }
        })
      } else {
        Messenger(
          { extraClasses: 'messenger-fixed messenger-on-right messenger-on-top' }
        ).post({
          message: this.translate.instant(message),
          type: type,
          showCloseButton: true
        });
      }
    } else {
      if (error.error){
        Messenger(
          { extraClasses: 'messenger-fixed messenger-on-right messenger-on-top' }
        ).post({
          message: this.translate.instant(error.error.error.message),
          type: type,
          showCloseButton: true
        });
      }      
    }
  }

}
