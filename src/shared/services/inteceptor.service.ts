import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class InteceptorService implements HttpInterceptor {
  tokenSubscription: any
  flag = false;
  constructor(private authService: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authService.checkToken()) {
      let body = this.authService.getToken()
      if (new Date(this.authService.getToken().exp) < new Date() && this.flag == false) {
        this.flag = true;
        body.accessToken = null;
        this.tokenSubscription ? this.tokenSubscription.unsubscribe() : null
        this.tokenSubscription = this.authService.refreshToken({ accessToken: body.accessToken }, body.id).subscribe(response => {
          this.flag = false;
          this.authService.setAuthData(response);
        })
      } else {
        let newHeader: HttpHeaders
        req.method != "HEAD"? newHeader = new HttpHeaders({ 'Authorization': 'Bearer ' + this.authService.getToken().accessToken, 'Refresh-Token': body.refreshToken  }):newHeader = new HttpHeaders({ 'Authorization': 'Bearer ' + this.authService.getToken().accessToken, 'Refresh-Token': body.refreshToken, 'No-Check':'true', 'Access-Control-Expose-Headers':'items-count' })
        let cloneReq = req.clone({ headers: newHeader })
        return next.handle(cloneReq)
      }
    } else {
      return next.handle(req)
    }
  }
}
