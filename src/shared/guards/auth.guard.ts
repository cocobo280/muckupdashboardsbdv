import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertService } from '../services/alert.service';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private alertService: AlertService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    // if (this.authService.guardAuth(state.url)) {
    //   let res = this.authService.guardAuth(state.url)
    //   return this.authService.guardAuth(state.url)
    // }
    // this.alertService.alert("alerts.accessDenied","info")
    // return false
    return true
  }

}
