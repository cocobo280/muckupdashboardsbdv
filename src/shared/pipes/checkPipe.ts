import { Pipe, PipeTransform } from '@angular/core';
import { FormGroup } from '@angular/forms';

type option = {
    label: string,
    value: any,
    condition?: condition
}

type condition = {
    relatedTo: string,
    value: any
}

@Pipe({
    name: 'check'
})

export class CheckPipe implements PipeTransform {

    transform(obj: any, formData: any): boolean {
        if (obj && Object.keys(formData).length) return this.checkCondition(obj, formData)
        else return false
    }

    checkCondition(obj, formData?: any): boolean {
        if (typeof obj == 'boolean') {
            return obj
        }
        else if (Array.isArray(obj)) {
            let condition = obj[0][0]
            if (condition == 'and') {
                let output = true;
                obj.forEach((el, index) => {
                    if (index > 0) {
                        output = output && this.checkCondition(el, formData)
                    }
                })
                return output
            } else {
                let output = false;
                obj.forEach((el, index) => {
                    if (index > 0) {
                        if (this.checkCondition(el, formData)) {
                            output = true;
                        }
                    }
                })
                return output
            }
        }
        else {
            let data;
            if (formData[obj.relatedTo]) {
                data = formData[obj.relatedTo]
            } else {
                return false
            }
            if ((data || data === false) && typeof data == 'object') {
                switch (obj.operator) {
                    case '==':
                        if (data == obj.value || data.indexOf(obj.value) != -1) {
                            return true
                        }
                        break;
                    case '!=':
                        if (data.indexOf(obj.value) == -1) {
                            return true
                        }
                        break;
                    default:
                        break;
                }
            } else if (data || data === false) {
                switch (obj.operator) {
                    case '==':
                        if (data == obj.value) {
                            return true
                        }
                        break;
                    case '!=':
                        if (data != obj.value) {
                            return true
                        }
                        break;
                    case '<':
                        if (data < obj.value) {
                            return true
                        }
                        break;
                    case '>':
                        if (data > obj.value) {
                            return true
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        return false
    }

}
