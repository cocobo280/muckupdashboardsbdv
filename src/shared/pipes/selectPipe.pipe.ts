import { array } from '@amcharts/amcharts4/core';
import { Pipe, PipeTransform } from '@angular/core';

type Option = { label: string, value: string };

@Pipe({
    name: 'selectPipe'
})
export class SelectPipe implements PipeTransform {

    transform(value: any, options: Option[]): string {
        if ((value || value === false) && options) {
            const  idx = options.find(el => el.value == value)
            return idx ? idx.label : value
        }
        return value
    }

}