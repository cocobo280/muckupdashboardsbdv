import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
    name: 'safeHtml'
})
export class SafeHtmlPipe implements PipeTransform {

    constructor(private sanitizer: DomSanitizer) { }

    transform(html) {
        if (html['text/html']) {
            return this.sanitizer.bypassSecurityTrustHtml(html['text/html']);            
        }
        if(html['text/plain']){
            return html['text/plain']
        }
        return   
    }

}