import { Pipe, PipeTransform } from '@angular/core';
const SUFFIXES = ['K', 'M', 'B', 'T'];
const DECIMAL_PLACES = 1;

@Pipe({
  name: 'shortn'
})
export class ShortNPipe implements PipeTransform {

  transform(value: any, decimalPlaces: number = DECIMAL_PLACES): any {
    let sign = '';
    const absoluteValue: number = Math.abs(+value);
    if (value < 0) {
      sign = '-';
    }
    return `${sign}${this.abbreviateNumber(absoluteValue, +decimalPlaces || DECIMAL_PLACES)}`;
  }

  abbreviateNumber(value: number, decPlaces: number = 0): string {
    let abbreviationNumber: string = Math.round(value).toString();
    let suffixesCount: number = SUFFIXES.length;

    decPlaces = Math.pow(10, decPlaces);

    for (let i = suffixesCount - 1; i >= 0; i--) {
      let size = Math.pow(10, (i + 1) * 3);

      if (size <= value) {
        abbreviationNumber = (Math.round(value * decPlaces / size) / decPlaces).toString();
        abbreviationNumber += SUFFIXES[i];

        break;
      }
    }

    return abbreviationNumber;
  }
}
