import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayToTitle'
})
export class ArrayToTitlePipe implements PipeTransform {

  transform(value: any[], word: string): string {
    let searchRegExp = new RegExp(word, 'g');
    return value.toString().replace(searchRegExp, ", ");
  }

}
