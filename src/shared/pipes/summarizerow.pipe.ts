import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'summarizerow'
})
export class SummarizerowPipe implements PipeTransform {

  transform(value: any, fields: string[]): number {
    if (!Array.isArray(fields)) return 0
    let total = fields.reduce((acc, el) => (value[el] ? acc += parseInt(value[el]) : acc += 0), 0);
    value.total = total;
    return total
  }

}
