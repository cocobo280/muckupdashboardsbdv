import { Pipe, PipeTransform } from '@angular/core';

type option = {
  label: string, 
  value: any, 
  condition?: condition
}

type condition = {
  relatedTo: string,
  value: any
}

@Pipe({
  name: 'selectdepends'
})
export class SelectDependsPipe implements PipeTransform {

  transform(value: option[], form: any): option[] {
    return value.filter(el => !el.condition || form[el.condition.relatedTo] == el.condition.value);
  }

}
