import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";

@Pipe({
  name: 'tradOptions'
})
export class TranslateoptionsPipe implements PipeTransform {

  constructor(private translateService: TranslateService) { }

  transform(value: any[], lang: string, label?: string): any[] {
    let result: { label: string, value: any }[] = []
    if (value.length) {
      let field = label ? label : 'label';
      this.translateService.use(lang || (localStorage.getItem('lang') || this.translateService.defaultLang));
      this.translateService.get(value.map(el => el[field])).subscribe(res => {
        result = value.map(el2 => ({ ...el2, [field]: res[el2[field]] }))
        return result
      })
    }
    return result;
  }
}