import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'hasValue'
})
export class HasValuePipe implements PipeTransform {

    transform(value: any): boolean {
        if (value) {
            if (typeof value == "string") return value != ""
            if (typeof value == "object" && Array.isArray(value)) return value.length != 0
        }
        return false
    }

}