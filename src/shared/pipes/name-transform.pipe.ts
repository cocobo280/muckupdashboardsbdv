import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'name'
})
export class NameTransformPipe implements PipeTransform {

  transform(value: string) {
    if (value) {
      return value.replace(/[a-z]/g, '').replace(' ', '').substr(0, 2);
    }
    return ""
  }
}
