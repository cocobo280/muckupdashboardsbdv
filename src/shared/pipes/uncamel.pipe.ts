import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'uncamel'
})
export class UncamelPipe implements PipeTransform {

  transform(value: string): any {
    const result = value.replace(/([A-Z])/g, " $1");
    return result.charAt(0).toUpperCase() + result.slice(1);
  }

}
