import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'minMaxD',
    pure: true
})
export class MinMaxDatePipe implements PipeTransform {
    transform(value: string): any {
        if (value) {
            if (value.charAt(0) == "-") return new Date(moment().subtract(parseInt(value.substr(1)), "days").startOf("day").format());
            if (value.charAt(0) == "+") return new Date(moment().add(parseInt(value.substr(1)), "days").endOf("day").format());            
        }
        return null
    }
}