import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'summarize'
})
export class SummarizePipe implements PipeTransform {

  transform(value: any[], field: string): number {
    return value.reduce((acc, el) => (el[field]? acc += parseFloat(el[field]): acc), 0);
  }

}
