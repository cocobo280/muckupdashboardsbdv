import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterPipe } from './filterPipe.pipe';
import { HasValuePipe } from "./hasValue.pipe";
import { PermmisionsPipe } from './permmisions.pipe';
import { MinMaxDatePipe } from './minMaxDate.pipe';
import { SafeHtmlPipe } from './safeHtml.pipe';
import { ArrayToTitlePipe } from './array-to-title.pipe';
import { CheckPipe } from './checkPipe';
import { SelectDependsPipe } from './selectDepends.pipe';
import { OrdeByPipe } from './orderBy';
import { ShortNPipe } from './shortN.pipe';
import { UncamelPipe } from './uncamel.pipe';
import { SummarizerowPipe } from './summarizerow.pipe';
import { SummarizePipe } from './summarize.pipe';

const pipes = [
  FilterPipe,
  HasValuePipe,
  PermmisionsPipe,
  MinMaxDatePipe,
  SafeHtmlPipe,
  ArrayToTitlePipe,
  CheckPipe,
  SelectDependsPipe,
  OrdeByPipe,
  ShortNPipe,
  UncamelPipe,
  SummarizePipe,
  SummarizerowPipe
]

@NgModule({
  declarations: [
    pipes,
    
  ],
  imports: [
    CommonModule
  ],
  exports: [
    pipes
  ]
})
export class PipeModule { }
