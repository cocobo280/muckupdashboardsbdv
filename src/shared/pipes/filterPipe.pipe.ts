import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterPipe'
})
export class FilterPipe implements PipeTransform {

    transform(array: any[], key?: any, value?: any): any {
        if (key) {
            if (array && typeof value == 'string') {
                return array.filter(el => { return el[key] != value })
            }
            if (array && typeof value == 'object') {
                return array.filter(el => { return !value.includes(el[key]) })
            } 
        }else{
            if (array && typeof value == 'string') {
                return array.filter(el => { return el != value })
            }
            if (array && typeof value == 'object') {
                return array.filter(el => { return !value.includes(el) })
            } 
        }
        
        return array;
    }

}