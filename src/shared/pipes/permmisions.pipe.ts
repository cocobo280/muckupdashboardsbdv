import { Pipe, PipeTransform } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Pipe({
    name: 'canDo'
})
export class PermmisionsPipe implements PipeTransform {
    constructor(private authService: AuthService) { }

    transform(module: string, action: string): boolean {  
        return this.authService.checkAuth(module, action);
    }

}