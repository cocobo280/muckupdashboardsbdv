import { AfterViewInit, Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.scss']
})
export class BarComponent implements OnChanges, OnDestroy, AfterViewInit {
  @Input() categoryField: string = 'category';
  @Input() valueField: string = 'value';
  @Input() categoryText: string = '';
  @Input() value: any[] = [];
  @Input() series: {
    name: string,
    field: string | number,
  }[] = [];
  @Input() chartId: string;
  @Input() title: string;
  firstTime = true;
  chart: am4charts.XYChart;

  constructor() { }

  ngAfterViewInit(): void {
    //this.createGauge(this.value);
  }

  ngOnChanges(changes: SimpleChanges): void {
    setTimeout(() => {
      this.createChart(this.value);
    }, 100);
  };

  createChart(data?: any) {
    if (!data && data != 0) return
    let chart = am4core.create(this.chartId + '-chart', am4charts.XYChart);
    chart.padding(20,60,20,10)
    // Add data
    chart.data = data;

    // Create axes
    let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = this.categoryField;
    categoryAxis.numberFormatter.numberFormat = "#";
    categoryAxis.renderer.inversed = true;
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.cellStartLocation = 0.1;
    categoryAxis.renderer.cellEndLocation = 0.9;

    let valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.opposite = true;

    // Create series
    const createSeries = (field, name) => {
      let series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueX = field;
      series.dataFields.categoryY = this.categoryField;
      series.name = name;
      series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
      series.columns.template.height = am4core.percent(100);
      series.sequencedInterpolation = true;

      let valueLabel = series.bullets.push(new am4charts.LabelBullet());
      valueLabel.label.text = "{valueX}";
      valueLabel.label.horizontalCenter = "left";
      valueLabel.label.dx = 10;
      valueLabel.label.hideOversized = false;
      valueLabel.label.truncate = false;

      let categoryLabel = series.bullets.push(new am4charts.LabelBullet());
      categoryLabel.label.text = "{name}";
      categoryLabel.label.horizontalCenter = "right";
      categoryLabel.label.dx = -10;
      categoryLabel.label.fill = am4core.color("#fff");
      categoryLabel.label.hideOversized = false;
      categoryLabel.label.truncate = false;
    }

    this.series.forEach(el => {
      createSeries(el.field, el.name);
    })
    chart.legend = new am4charts.Legend();
    this.chart = chart;
  }

  ngOnDestroy(): void {
    this.chart.dispose();
  }

}
