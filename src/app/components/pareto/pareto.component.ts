import { AfterViewInit, Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

@Component({
  selector: 'app-pareto',
  templateUrl: './pareto.component.html',
  styleUrls: ['./pareto.component.scss']
})
export class ParetoComponent implements OnChanges, OnDestroy, AfterViewInit {
  @Input() categoryField: string = 'category';
  @Input() valueField: string = 'value';
  @Input() categoryText: string = '';
  @Input() value: {
    name: string,
    value1: number
    value2: number
  }[] = [];
  @Input() series: {
    name: string,
    field: string,
  }[] = [];
  @Input() chartId: string;
  @Input() title: string;
  firstTime = true;
  chart: am4charts.XYChart;

  constructor() { }

  ngAfterViewInit(): void {
    //this.createGauge(this.value);
  }

  ngOnChanges(changes: SimpleChanges): void {
    setTimeout(() => {
      this.createChart(this.value);
    }, 100);
  };

  createChart(data?: any) {
    if (!data && data != 0) return
    let chart = am4core.create(this.chartId + '-chart', am4charts.XYChart);
    chart.scrollbarX = new am4core.Scrollbar();
    // Add data
    chart.data = data;

    // Create axes
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = this.categoryField;
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 60;
    categoryAxis.tooltip.disabled = true;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.minWidth = 50;
    valueAxis.min = 0;
    valueAxis.cursorTooltipEnabled = false;

    // Create series
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.sequencedInterpolation = true;
    series.dataFields.valueY = this.valueField;
    series.dataFields.categoryX = this.categoryField;
    series.tooltipText = this.categoryText ? this.categoryText + ": {valueY}" : "{valueY}";
    series.columns.template.strokeWidth = 0;

    series.tooltip.pointerOrientation = "vertical";

    series.columns.template.column.cornerRadiusTopLeft = 10;
    series.columns.template.column.cornerRadiusTopRight = 10;
    series.columns.template.column.fillOpacity = 0.8;

    // on hover, make corner radiuses bigger
    let hoverState = series.columns.template.column.states.create("hover");
    hoverState.properties.cornerRadiusTopLeft = 0;
    hoverState.properties.cornerRadiusTopRight = 0;
    hoverState.properties.fillOpacity = 1;

    series.columns.template.adapter.add("fill", function (fill, target) {
      return chart.colors.getIndex(target.dataItem.index);
    })

    let paretoValueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    paretoValueAxis.renderer.opposite = true;
    paretoValueAxis.min = 0;
    //paretoValueAxis.max = 100;
    //paretoValueAxis.strictMinMax = true;
    paretoValueAxis.renderer.grid.template.disabled = true;
    paretoValueAxis.numberFormatter = new am4core.NumberFormatter();
    //paretoValueAxis.numberFormatter.numberFormat = "#'%'"
    paretoValueAxis.cursorTooltipEnabled = false;
    this.series.forEach(serie => {
      let paretoSeries = chart.series.push(new am4charts.LineSeries())
      paretoSeries.dataFields.valueY = serie.field;
      paretoSeries.dataFields.categoryX = this.categoryField;
      paretoSeries.yAxis = paretoValueAxis;
      paretoSeries.tooltipText = serie.name + ": {valueY}";
      paretoSeries.bullets.push(new am4charts.CircleBullet());
      paretoSeries.strokeWidth = 2;
      paretoSeries.stroke = new am4core.InterfaceColorSet().getFor("alternativeBackground");
      paretoSeries.strokeOpacity = 0.5;
    })

    // Cursor
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "panX";
    chart.legend = new am4charts.Legend();
    this.chart = chart;
  }

  ngOnDestroy(): void {
    this.chart.dispose();
  }

}
