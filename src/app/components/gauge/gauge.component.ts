import { Component, OnDestroy, Input, OnChanges, SimpleChanges, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import { Axis, AxisRendererCircular } from '@amcharts/amcharts4/charts';

@Component({
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class GaugeComponent implements OnChanges, OnDestroy, AfterViewInit {
  @Input() value: number;
  @Input() chartId: string;
  firstTime = true;

  constructor() { }

  ngAfterViewInit(): void {
    //this.createGauge(this.value);
  }

  ngOnChanges(changes: SimpleChanges): void {
    setTimeout(() => {
      this.createGauge(this.value);
    }, 100);
  };

  createGauge(data?: any) {
    if (!data && data != 0) return
    let chart = am4core.create(this.chartId + '-chart', am4charts.GaugeChart);
    data = Math.round(data);
    chart.innerRadius = am4core.percent(82);
    chart.padding(0, 0, 10, 0);
    chart.startAngle = -180;
    chart.endAngle = 0;
    // chart.height = 90;
    // chart.width = 180;

    let axis2 = chart.xAxes.push(new am4charts.ValueAxis() as Axis<AxisRendererCircular>);
    axis2['min'] = 0;
    axis2['max'] = 100;
    axis2['strictMinMax'] = true;
    axis2.renderer.labels.template.disabled = true;
    axis2.renderer.grid.template.disabled = true;

    let range0 = axis2.axisRanges.create();
    range0['value'] = 0;
    range0['endValue'] = parseInt((parseInt(data) > 100 ? 100 : data));
    range0.axisFill.fillOpacity = 1;
    range0.axisFill.fill = am4core.color('#97C701');

    let range1 = axis2.axisRanges.create();
    range1['value'] = parseInt((parseInt(data) > 100 ? 100 : data));
    range1['endValue'] = 100;
    range1.axisFill.fillOpacity = 0.8;
    range1.axisFill.fill = am4core.color('#000000');

    /**
     * Label
     */

    let label = chart.radarContainer.createChild(am4core.Label);
    label.isMeasured = false;
    label.fontSize = 18;
    label.x = am4core.percent(50);
    label.y = 22;
    label.horizontalCenter = "middle";
    label.verticalCenter = "bottom";
    label.text = `${data}%`;

    let hand = chart.hands.push(new am4charts.ClockHand());
    // hand.axis = axis2;
    // hand.innerRadius = am4core.percent(78);
    // hand.radius = am4core.percent(103);
    // hand.endWidth = 6;
    // hand.startWidth = 6;
    // hand.stroke = am4core.color('#DEDE00');
    // hand.fill = am4core.color('#DEDE00');
    // hand.pin.disabled = true;
    // hand.value = value;


    hand.axis = axis2;
    hand.innerRadius = am4core.percent(35);
    hand.startWidth = 8;
    hand.pin.disabled = true;
    hand.radius = am4core.percent(103);
    hand.value = parseInt((parseInt(data) > 100 ? 100 : data));
  }

  ngOnDestroy(): void {
    am4core.disposeAllCharts();
  }

}
