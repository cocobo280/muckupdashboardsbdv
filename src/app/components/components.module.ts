import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from "./sidebar/sidebar.component";
import { SidebarListItemComponent } from './sidebar/list-item/list-item.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { ParetoComponent } from './pareto/pareto.component';
import { BarComponent } from './bar/bar.component';
import { VarianceComponent } from './variance/variance.component';

const components = [
  SidebarComponent,
  SidebarListItemComponent,
  NavbarComponent,
  ParetoComponent,
  BarComponent,
  VarianceComponent
]

@NgModule({
  declarations: [
    components
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    components
  ]
})

export class ComponentsModule { }