import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Cliente, clientes, Grupo, grupos } from "./data";

@Injectable({
  providedIn: 'root'
})
export class DashboardFiltersService {
  segment = new BehaviorSubject<{ label: string, value: any }[]>([
    { label: 'CORPORATIVA', value: 'CORPORATIVA' },
    { label: 'PYMES', value: 'PYMES' }
  ]);
  clients = new BehaviorSubject<{ label: string, value: any, data:any }[]>((clientes.map(el => ({ label: `${el.NOMBRE_CLIENTE} ${el.RIF_CEDULA}`, value: el.RIF_CEDULA, data:el }))));
  groups = new BehaviorSubject<{ label: string, value: any }[]>((grupos.map(el => ({ label: el.NOMBRE_GRUPO, value: el.COD_GRUPO }))));
  currentSegment: EventEmitter<string> = new EventEmitter();
  currentGroup: EventEmitter<string> = new EventEmitter();
  currentClient: EventEmitter<string> = new EventEmitter();

  constructor() { }

  filterBySegment(segment:string){
    let clients = clientes.filter(el => (el.SEGMENTO == segment)).map(el => ({ label: `${el.NOMBRE_CLIENTE} ${el.RIF_CEDULA}`, value: el.RIF_CEDULA, data:el }));
    this.clients.next(clients);
  };

  filterByGroup(group){
    let clients = clientes.filter(el => (el.GRUPO_ECONOMICO == group)).map(el => ({ label: `${el.NOMBRE_CLIENTE} ${el.RIF_CEDULA}`, value: el.RIF_CEDULA, data:el }));
    this.clients.next(clients);
  };

}
