import { Component, AfterViewInit, Output, Input, EventEmitter, OnDestroy } from '@angular/core';
import { DashboardFiltersService } from './dashboard-filters.service';
import { Cliente } from './data';

@Component({
  selector: 'app-dashbords-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements AfterViewInit, OnDestroy {
  segmentos$: any;
  segment;
  segmentos: { label: string, value: any }[] = [];
  grupos$: any;
  group;
  grupos: { label: string, value: any }[] = [];
  clientes$: any;
  client;
  clientes: { label: string, value: any, data: any }[] = [];

  @Output() onSegmentSelect: EventEmitter<string> = new EventEmitter();
  @Output() onGroupSelect: EventEmitter<string> = new EventEmitter();
  @Output() onClientSelect: EventEmitter<Cliente> = new EventEmitter();

  constructor(private fliterService: DashboardFiltersService) {
    this.segmentos$ = this.fliterService.segment.subscribe(ev => {
      this.segmentos = ev;
    })
    this.grupos$ = this.fliterService.groups.subscribe(ev => {
      this.grupos = ev;
    })
    this.clientes$ = this.fliterService.clients.subscribe(ev => {
      this.clientes = ev;
    })
  }

  ngAfterViewInit() {

  }

  onSegmentChange(event) {
    this.client = undefined;
    this.fliterService.filterBySegment(this.segment);
    this.onSegmentSelect.emit(this.segment);
    this.onClientSelect.emit({});
  };

  onGroupChange(event) {
    this.client = undefined;
    this.fliterService.filterByGroup(this.group);
    this.onGroupSelect.emit(this.group);
    this.onClientSelect.emit({});
  };

  onClientChange(event) {
    const { data } = event;console.log(data);
    this.onClientSelect.emit(data || {});
  };

  ngOnDestroy(): void {
    this.segmentos$ ? this.segmentos$.unsubscribe() : null;
    this.grupos$ ? this.grupos$.unsubscribe() : null;
    this.clientes$ ? this.clientes$.unsubscribe() : null;
  }
}
