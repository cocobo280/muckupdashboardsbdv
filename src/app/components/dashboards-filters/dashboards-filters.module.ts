import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltersComponent } from './filters.component';
import { FormsModule } from '@angular/forms';
import { PrimengModule } from '../../../shared/primeng/primeng.module';
import { PipeModule } from '../../../shared/pipes/pipe.module';
import { DashboardFiltersService } from "./dashboard-filters.service";

@NgModule({
  declarations: [
    FiltersComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PrimengModule,
    PipeModule
  ],
  exports: [
    FiltersComponent
  ],
  providers:[
    DashboardFiltersService
  ]
})
export class DashboardsFiltersModule { }
