import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { MenuItem } from "./types";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() menuItems: MenuItem[] = [];
  @Input() toggle: boolean = false;
  @Output() toggleChange = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit(): void {
    this.setMenu();
  }

  setMenu() {
    this.menuItems = [
      {
        label: 'Dashboards',
        visible: true,
        icon: 'pi pi users',
        items: [
          {
            label: 'Grupo Económico',
            routerLink: 'client-economic-group'
          },
          {
            label: 'Segmento',
            routerLink: 'client-segement'
          },
          {
            label: 'Pasivos y Créditos',
            routerLink: 'passives-credits'
          },
          {
            label: 'Depósitos y Compras',
            routerLink: 'deposits-purchases'
          },
          {
            label: 'Productos Vinculados',
            routerLink: 'linked-products'
          }
        ]
      }
    ]
  }

  emitToggle() {
    this.toggleChange.emit(this.toggle)
  }
}
