import { Component, EventEmitter, Input, Output, OnDestroy } from '@angular/core';
import { MenuItem } from "../types";

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['../sidebar.component.scss']
})
export class SidebarListItemComponent implements OnDestroy {
  @Input() items: MenuItem;

  constructor() {
  }

  ngOnDestroy(): void {
  }

}
