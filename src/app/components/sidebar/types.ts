export type MenuItem = {
    label?: string,
    routerLink?: string,
    visible?: boolean,
    icon?: string,
    items?: MenuItem[]
}