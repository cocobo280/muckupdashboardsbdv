import { AfterViewInit, Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

@Component({
  selector: 'app-variance',
  templateUrl: './variance.component.html',
  styleUrls: ['./variance.component.scss']
})
export class VarianceComponent implements OnChanges, OnDestroy, AfterViewInit {
  @Input() categoryField: string = 'category';
  @Input() valueField: string = 'value';
  @Input() categoryText: string = '';
  @Input() value: any[] = [];
  @Input() series: {
    name: string,
    field: string | number,
  }[] = [];
  @Input() chartId: string;
  @Input() title: string;
  firstTime = true;
  chart: am4charts.XYChart;

  constructor() { }

  ngAfterViewInit(): void {
    //this.createGauge(this.value);
  }

  ngOnChanges(changes: SimpleChanges): void {
    setTimeout(() => {
      this.createChart(this.value);
    }, 100);
  };

  createChart(data?: any) {
    if (!data && data != 0) return
    let chart = am4core.create(this.chartId + '-chart', am4charts.XYChart);
    // Add data
    chart.data = data;
    
    // Populate data
    for (var i = 0; i < (chart.data.length - 1); i++) {
      chart.data[i].valueNext = chart.data[i + 1][this.valueField];
    }

    // Create axes
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = this.categoryField;
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;

    // Create series
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = this.valueField;
    series.dataFields.categoryX = this.categoryField;

    // Add series for showing variance arrows
    let series2 = chart.series.push(new am4charts.ColumnSeries());
    series2.dataFields.valueY = "valueNext";
    series2.dataFields.openValueY = this.valueField;
    series2.dataFields.categoryX = this.categoryField;
    series2.columns.template.width = 1;
    series2.fill = am4core.color("#555");
    series2.stroke = am4core.color("#555");

    // Add a triangle for arrow tip
    let arrow = series2.bullets.push(new am4core.Triangle);
    arrow.width = 10;
    arrow.height = 10;
    arrow.horizontalCenter = "middle";
    arrow.verticalCenter = "top";
    arrow.dy = -1;

    // Set up a rotation adapter which would rotate the triangle if its a negative change
    arrow.adapter.add("rotation", function (rotation, target) {
      return getVariancePercent(target.dataItem) < 0 ? 180 : rotation;
    });

    // Set up a rotation adapter which adjusts Y position
    arrow.adapter.add("dy", function (dy, target) {
      return getVariancePercent(target.dataItem) < 0 ? 1 : dy;
    });

    // Add a label
    let label = series2.bullets.push(new am4core.Label);
    label.padding(10, 10, 10, 10);
    label.text = "";
    label.fill = am4core.color("#0c0");
    label.strokeWidth = 0;
    label.horizontalCenter = "middle";
    label.verticalCenter = "bottom";
    label.fontWeight = "bolder";

    // Adapter for label text which calculates change in percent
    label.adapter.add("textOutput", function (text, target) {
      let percent = getVariancePercent(target.dataItem);
      return percent ? percent + "%" : text;
    });

    // Adapter which shifts the label if it's below the variance column
    label.adapter.add("verticalCenter", function (center, target) {
      return getVariancePercent(target.dataItem) < 0 ? "top" : center;
    });

    // Adapter which changes color of label to red
    label.adapter.add("fill", function (fill, target) {
      return getVariancePercent(target.dataItem) < 0 ? am4core.color("#c00") : fill;
    });

    function getVariancePercent(dataItem) {
      if (dataItem) {
        let value = dataItem.valueY;
        let openValue = dataItem.openValueY;
        let change = value - openValue;
        return Math.round(change / openValue * 100);
      }
      return 0;
    }

    chart.legend = new am4charts.Legend();
    this.chart = chart;
  }

  ngOnDestroy(): void {
    this.chart.dispose();
  }

}
