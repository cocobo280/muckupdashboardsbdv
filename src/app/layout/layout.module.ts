import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routes } from "./layout-routing.module";
import { RouterModule } from '@angular/router';

//modules
import { ComponentsModule } from '../components/components.module';

//components
import { LayoutComponent } from './layout.component';
import { ClientEconomicGroupComponent } from '../pages/dashboards/client-economic-group/client-economic-group.component';
import { ClientSegementComponent } from '../pages/dashboards/client-segement/client-segement.component';
import { DepositsPurchasesComponent } from '../pages/dashboards/deposits-purchases/deposits-purchases.component';
import { LinkedProductsComponent } from '../pages/dashboards/linked-products/linked-products.component';
import { PassivesCreditsComponent } from '../pages/dashboards/passives-credits/passives-credits.component';
import { PipesModule } from '../pages/dashboards/utilities/pipes/pipes.module';
import { GaugeModule } from '../components/gauge/gauge.module';
import { RadarModule } from '../components/radar/radar.module';
import { DashboardsFiltersModule } from '../components/dashboards-filters/dashboards-filters.module';



@NgModule({
  declarations: [
    LayoutComponent,
    ClientSegementComponent,
    ClientEconomicGroupComponent,
    PassivesCreditsComponent,
    DepositsPurchasesComponent,
    LinkedProductsComponent,],
  imports: [
    CommonModule,
    ComponentsModule,
    PipesModule,
    GaugeModule,
    RadarModule,
    DashboardsFiltersModule,
    RouterModule.forChild(routes)
  ]
})
export class LayoutModule { }
