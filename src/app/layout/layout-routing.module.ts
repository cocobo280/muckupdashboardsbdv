import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { ClientEconomicGroupComponent } from '../pages/dashboards/client-economic-group/client-economic-group.component';
import { ClientSegementComponent } from '../pages/dashboards/client-segement/client-segement.component';
import { DepositsPurchasesComponent } from '../pages/dashboards/deposits-purchases/deposits-purchases.component';
import { LinkedProductsComponent } from '../pages/dashboards/linked-products/linked-products.component';
import { PassivesCreditsComponent } from '../pages/dashboards/passives-credits/passives-credits.component';

export const routes: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      { path: '', redirectTo: 'landing', pathMatch: 'full' },
      { path: 'landing', loadChildren: () => import('../pages/landing/landing.module').then(mod => mod.LandingModule) },
      { path: 'client-economic-group', component: ClientEconomicGroupComponent, pathMatch: 'full' },
      { path: 'client-segement', component: ClientSegementComponent, pathMatch: 'full' },
      { path: 'deposits-purchases', component: DepositsPurchasesComponent, pathMatch: 'full' },
      { path: 'linked-products', component: LinkedProductsComponent, pathMatch: 'full' },
      { path: 'passives-credits', component: PassivesCreditsComponent, pathMatch: 'full' }
    ]
  }
];
