import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Cliente } from 'src/app/components/dashboards-filters/data';
import { RandomPipe } from "../utilities/pipes/random.pipe";

@Component({
  selector: 'app-passives-credits',
  templateUrl: './passives-credits.component.html',
  styleUrls: ['./passives-credits.component.scss'],
  providers: [RandomPipe]
})
export class PassivesCreditsComponent implements OnInit {

  series: {
    name: string,
    field: string,
  }[] = [
      {
        name: 'Test',
        field: 'visits'
      }
    ];
  chartData: any[] = [];
  segment: string = '';
  group: string = '';
  client: Cliente = {};
  months = {
    0: 'ENE',
    1: 'FEB',
    2: 'MAR',
    3: 'ABR',
    4: 'MAY',
    5: 'JUN',
    6: 'JUL',
    7: 'AGO',
    8: 'SEP',
    9: 'OCT',
    10: 'NOV',
    11: 'DIC',
  }


  constructor(private randomPipe: RandomPipe) { }

  ngOnInit(): void {
  }

  getChartData() {
    this.series = [
      {
        name: 'Saldo promedio',
        field: 'value2',
      },
      {
        name: 'Monto abonado',
        field: 'value3',
      }
    ];
    this.chartData = Array.from({ length: new Date().getMonth() + 1 }, (x, el) => {
      let value = Math.round(this.randomPipe.transform(0, 95, 1000));
      let value2 = Math.round(value * (this.randomPipe.transform(100, 600) / 1000));
      let value3 = value2 * 0.1;
      return {
        category: this.months[moment(moment().startOf('year')).add(el, 'months').month()],
        value: value,
        value2: value2,
        value3: value3,
      }
    })
    console.log(this.chartData);

  }

}
