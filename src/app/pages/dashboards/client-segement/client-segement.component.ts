import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/components/dashboards-filters/data';
import { RandomPipe } from "../utilities/pipes/random.pipe";

@Component({
  selector: 'app-client-segement',
  templateUrl: './client-segement.component.html',
  styleUrls: ['./client-segement.component.scss'],
  providers: [RandomPipe]
})
export class ClientSegementComponent implements OnInit {
  radarCategories: string[] = [
    'Crecimiento Anual Monto Abonado',
    'Crecimiento Anual Saldo Promedio',
    'Cartera Crédito Sep-22',
    'Reciprocidad del Segmento',
    'Reciprocidad Sep 2022',
    'Monto Otorgado Acumulado'
  ];
  radarData: {
    name: string,
    value1: number
    value2: number
  }[] = [];

  radarSeries: {
    name: string,
    categoryX: string,
    valueY: string,
  }[] = [];

  segment: string = '';
  group: string = '';
  client: Cliente = {};


  constructor(private randomPipe: RandomPipe) { }

  ngOnInit(): void {
  }

  getRadarData() {
    this.radarSeries = [
      {
        name: this.client.NOMBRE_CLIENTE,
        categoryX: 'name',
        valueY: "value1"
      },
      {
        name: this.client.SEGMENTO,
        categoryX: 'name',
        valueY: "value2"
      }
    ];
    this.radarData = Array.from({ length: this.radarCategories.length }, (x, el) => {
      let value1 = Math.round(this.randomPipe.transform(0, 95, 1000));
      return {
        name: this.radarCategories[el],
        value1: value1,
        value2: value1 * Math.round(this.randomPipe.transform(25,55))
      }
    })
  }



}
