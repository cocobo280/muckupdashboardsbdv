import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Cliente } from 'src/app/components/dashboards-filters/data';
import { RandomPipe } from "../utilities/pipes/random.pipe";


@Component({
  selector: 'app-deposits-purchases',
  templateUrl: './deposits-purchases.component.html',
  styleUrls: ['./deposits-purchases.component.scss'],
  providers: [RandomPipe]
})
export class DepositsPurchasesComponent implements OnInit {

  chartData: any[] = [];
  chartSeries: {
    name: string,
    field: string
  }[] = []
  segment: string = '';
  group: string = '';
  client: Cliente = {};
  type: 'deposits' | 'buys' = 'deposits';  
  months = {
    0: 'Enero',
    1: 'Febrero',
    2: 'Marzo',
    3: 'Abril',
    4: 'Mayo',
    5: 'Junio',
    6: 'Julio',
    7: 'Agosto',
    8: 'Septiembre',
    9: 'Octubre',
    10: 'Noviembre',
    11: 'Diciembre',
  };

  constructor(private randomPipe: RandomPipe) { }

  ngOnInit(): void {
  }

  getChartData() {
    this.chartSeries = [
      {
        name: 'Dolares',
        field: 'dolar'
      },
      {
        name: 'Euros',
        field: 'euro'
      }
    ];
    this.chartData = Array.from({ length: new Date().getMonth() + 1 }, (x, el) => {
      let value = this.type == 'deposits'? Math.round(this.randomPipe.transform(350000, 1000000)) : Math.round(this.randomPipe.transform(99999, 500000));
      let value2 = Math.round(value * (this.randomPipe.transform(100, 600) / 1000));
      return {
        category: this.months[moment(moment().startOf('year')).add(el, 'months').month()],
        dolar: value,
        euro: value2,
      }
    })

    console.log(this.chartData);

  }

}
