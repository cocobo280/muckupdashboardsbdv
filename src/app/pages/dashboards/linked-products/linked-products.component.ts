import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Cliente } from 'src/app/components/dashboards-filters/data';
import { RandomPipe } from "../utilities/pipes/random.pipe";

@Component({
  selector: 'app-linked-products',
  templateUrl: './linked-products.component.html',
  styleUrls: ['./linked-products.component.scss'],
  providers: [RandomPipe]
})
export class LinkedProductsComponent implements OnInit {
  chartData: any[] = [];
  chartData2: any[] = [];
  chartSeries: {
    name: string,
    field: string | number
  }[] = [];
  segment: string = '';
  group: string = '';
  client: Cliente = {};
  type: 'volumen' | 'number' = 'volumen';
  months = {
    0: 'ENE',
    1: 'FEB',
    2: 'MAR',
    3: 'ABR',
    4: 'MAY',
    5: 'JUN',
    6: 'JUL',
    7: 'AGO',
    8: 'SEP',
    9: 'OCT',
    10: 'NOV',
    11: 'DIC'
  };

  constructor(private randomPipe: RandomPipe) { }

  ngOnInit(): void {
  }

  getChartData() {
    this.chartSeries = [
      {
        name: 'category',
        field: 'value'
      }];
    this.chartData = this.client.PRODUCTOS_VINCULADOS.filter(el => el.ACTIVO).reduce((acc, el) => {
      return acc.concat({
        category: el.PRODUCTO,
        value: Math.round(this.randomPipe.transform(1000, 50000))
      })
    }, []);
    this.chartData2 = Array.from({ length: new Date().getMonth() + 1 }, (x, el) => {
      let value = this.type == 'volumen' ? Math.round(this.randomPipe.transform(100000, 1000000)) : Math.round(this.randomPipe.transform(10, 60));
      return {
        category: this.months[moment(moment().startOf('year')).add(el, 'months').month()],
        value: value
      }
    })
  }


}