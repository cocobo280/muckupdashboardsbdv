import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'random'
})
export class RandomPipe implements PipeTransform {

  transform(value: number, min?: number, max?:number, change?:any): number {
    return value = Math.random() * ((max || 0) - (min || 0)) + (min || 0);
  }

}
