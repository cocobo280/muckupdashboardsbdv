import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RandomPipe } from './random.pipe';

const pipes = [
  RandomPipe
]

@NgModule({
  declarations: [pipes],
  imports: [
    CommonModule
  ],
  exports: [pipes]
})
export class PipesModule { }
