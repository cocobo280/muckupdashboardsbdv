import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'cosa', pathMatch: 'full'
  },
  {
    path: 'cosa', loadChildren: () => import('./layout/layout.module').then(mod => mod.LayoutModule)
  },
  {
    path: 'login', component: LoginComponent, pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
